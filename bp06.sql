--1 case izraz
select 
  concat(ime, ' ' || prezime),
  case spol
  when 0 then
    'muško'
  when 1 then
    'žensko'
  else
    'nepoznato'
  end as spol
from 
  studenti;

--2 case adrese
select 
   case 
   when mjesto = opcina then 
      postanskibroj || ' ' || opcina
   else 
      mjesto || ' ' || postanskibroj || ' ' || opcina
   end as adresa 
from 
   common.naselja;

--3 godište 1995
select 
  godiste, common.korisnici.* 
from 
  common.korisnici
where
  godiste= 1995;

--4 between
select 
   godiste, common.korisnici.*
from 
   common.korisnici
where 
   godiste between 1995 and 1997;

--5 in
select 
   godiste, common.korisnici.*
from 
   common.korisnici
where 
   godiste in(1995,1996,1997);

--6 OR 
select 
   godiste, common.korisnici.*
from 
   common.korisnici
where 
   godiste = 1995 OR 
   godiste = 1997;

--7 in
select 
   godiste, common.korisnici.*
from 
   common.korisnici
where 
   godiste in(1995, 1997);

--8 != AND
select 
   godiste, common.korisnici.*
from 
   common.korisnici
where 
   godiste != 1995 AND
   godiste != 1997;

--9 not in
select 
   godiste, common.korisnici.*
from 
   common.korisnici
where 
   godiste not in (1995, 1997);

--10 mjesto i općina imaju istu naziv
select 
  mjesto, opcina 
from 
   common.naselja 
where 
   opcina = mjesto;

--11 koliko mjesta i općina imaju isti naziv
select 
  count(1)
from 
   common.naselja 
where 
   opcina = mjesto; 

--12 mjesto i općina nemaju isti naziv
select 
  mjesto, opcina 
from 
   common.naselja 
where 
   opcina != mjesto;

--13 koliko mjesta i općina nemaju isti naziv
select 
  count(1)
from 
   common.naselja 
where 
   opcina != mjesto; 

--14 koliko je ukupno naselja
select 
  count(1)
from 
   common.naselja;

--15 matematika
select 6168 + 842 from dual;

--16 like operator
select 
   * 
from 
   common.korisnici
where 
   ime like 'Iv%';

--17 like s određenim brojem znakova
select 
   *
from 
   common.naselja
where 
   mjesto like 'B_______';

--18 like sa slovom na određenom mjestu
select 
   *
from 
   common.naselja
where 
   upper(mjesto) like '___L%';

--19 order by asc, asc
select  
   ime, prezime, godiste 
from 
   common.korisnici
where
  to_number(to_char(sysdate, 'YYYY')) - GODISTE < 30
order by 
   prezime asc, godiste asc;

--20 order by desc, asc
select 
   ime, prezime, godiste 
from 
   common.korisnici
where
  to_number(to_char(sysdate, 'YYYY')) - GODISTE < 30
order by 
   prezime desc, godiste asc;

--21 order by desc, desc
select 
   ime, prezime, godiste 
from 
   common.korisnici
where
  to_number(to_char(sysdate, 'YYYY')) - GODISTE < 30
order by 
   prezime desc, godiste desc;

--22 order by asc, desc
select 
   ime, prezime, godiste 
from 
   common.korisnici
where
  to_number(to_char(sysdate, 'YYYY')) - GODISTE < 30
order by 
   prezime asc, godiste desc;

--23 rownum
select 
   rownum, ime, prezime, godiste 
from 
   common.korisnici
where
  to_number(to_char(sysdate, 'YYYY')) - GODISTE < 30
order by 
   prezime asc, godiste desc;

--24 row_number()
select 
   rownum,
   row_number() over(order by prezime asc, godiste desc) as redoslijed, 
   ime, 
   prezime, 
   godiste 
from 
   common.korisnici
where
  to_number(to_char(sysdate, 'YYYY')) - GODISTE < 30;

--25 pagination
select * from (
   select 
      rownum,
      row_number() over(order by prezime asc, godiste desc) as redoslijed, 
      ime, 
      prezime, 
      godiste 
   from 
      common.korisnici
   where
      to_number(to_char(sysdate, 'YYYY')) - GODISTE < 30
   )
where 
   redoslijed > 50 and  
   redoslijed < 61;
   
--26 order by, offset, rows fetch next, rows only
select 
   rownum, 
   ime, 
   prezime, 
   godiste 
from 
   common.korisnici
where
   to_number(to_char(sysdate, 'YYYY')) - GODISTE < 30 
order by 
   prezime asc, godiste desc
OFFSET 
   50
ROWS FETCH NEXT 
   10
rows only;