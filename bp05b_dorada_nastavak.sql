--1
select 
   concat(ime || ' ', prezime) as naziv 
from 
  studenti;
  
--2
select 
   concat(ime, concat(' ', prezime)) as naziv 
from 
  studenti;
  
--3
select 
   concat(ime, lpad(prezime, 1)) as naziv 
from 
   studenti;
   
--4
select 
   concat(lpad(pozmob, 3, '0'), ' ' || mobitel) as mobitel 
from 
   studenti;
   
--5
select 
   rtrim(prezime,'ić') as prezime 
from 
   studenti;
   
--6
select 
   initcap(lower(ime)) || ' ' || 
   initcap(lower(prezime)) 
from 
   studenti;
   
--7
select 
   prezime, 
   length(rtrim(prezime)) duljina 
from 
   studenti;
   
--8 
select 
  max(length(rtrim(prezime))) maksimalno,
  min(length(rtrim(prezime))) minimalno
from 
   studenti;

--9
select 
  replace(prezime, 'ć', 'c') 
from 
  studenti;
  
--10
select 
  replace(
     replace(
        replace(
           replace(
              replace(
                 lower(prezime), 'ć', 'c'), 
                 'č', 'c'), 
                 'š', 's'), 
                 'đ', 'd'), 
                 'ž', 'z') 
from 
   studenti;
   
--11
select 
  substr(lower(ime), 0,1) 
from 
  studenti;
  
--12
select
   replace(
     replace(
        replace(
           replace(
              replace(
                 substr(lower(ime), 0,1), 'ć', 'c'), 
                 'č', 'c'), 
                 'š', 's'), 
                 'đ', 'd'), 
                 'ž', 'z') ||
   replace(
     replace(
        replace(
           replace(
              replace(
                 lower(prezime), 'ć', 'c'), 
                 'č', 'c'), 
                 'š', 's'), 
                 'đ', 'd'), 
                 'ž', 'z') as username
   
from 
  studenti;
  
--13
select 
   lower(e_mail),
   instr(e_mail,'@') as "gje_je_@"
from 
   studenti;
--14   
   
select 
   lower(e_mail),
   lower(substr(e_mail,0,(instr(e_mail,'@')) - 1))
from 
   studenti;
--15
select 
   distinct(godiste) 
from 
   studenti;
--16
select 
   * 
from
   studenti
where
   ime like 'S%';
--17
select 
   *
from 
   studenti
where 
  ime like '_a%';