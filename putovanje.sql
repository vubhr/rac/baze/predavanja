drop table putnici;
drop table autobusi;

CREATE TABLE autobusi( 
  id number(6) NOT NULL PRIMARY KEY,
  reg_oznaka varchar2(10) NOT NULL,
  cijena number(5,2) NOT NULL,
  CONSTRAINT autobusi_reg_uq UNIQUE
  (reg_oznaka)
);

CREATE TABLE putnici ( 
  id number(6) NOT NULL PRIMARY KEY,
  JMBAG number(8) NOT NULL,
  IDautobusa number(6),
  ime varchar2(50) NOT NULL,
  prezime varchar2(50) NOT NULL,
  e_mail  varchar2(50) NOT NULL,
  CONSTRAINT UQ_JMBAG UNIQUE (JMBAG),
  CONSTRAINT fk_autobus FOREIGN
  KEY(IDautobusa)
  references autobusi(id)
);


Insert into AUTOBUSI (ID,REG_OZNAKA,CIJENA) values (1,'BJ 472 CD','199,99');
Insert into AUTOBUSI (ID,REG_OZNAKA,CIJENA) values (2,'BJ 043 GB','359,99');
Insert into AUTOBUSI (ID,REG_OZNAKA,CIJENA) values (3,'BJ 654 MK','300');
Insert into AUTOBUSI (ID,REG_OZNAKA,CIJENA) values (4,'BJ 332 TS','145,45');
Insert into AUTOBUSI (ID,REG_OZNAKA,CIJENA) values (5,'BJ 123 IC','589');



Insert into putnici (ID,JMBAG,IDautobusa,IME,PREZIME,E_MAIL) values (1,36985478,1,'Mia','Horvat','mhorvat@vub.hr');
Insert into putnici (ID,JMBAG,IDautobusa,IME,PREZIME,E_MAIL) values (2,36985479,1,'Lucija','Kovačević','lkovacevic@vub.hr');
Insert into putnici (ID,JMBAG,IDautobusa,IME,PREZIME,E_MAIL) values (3,36985480,1,'Ema','Babić','ebabic@vub.hr');
Insert into putnici (ID,JMBAG,IDautobusa,IME,PREZIME,E_MAIL) values (4,36985481,1,'Ana','Marić','amaric@vub.hr');
Insert into putnici (ID,JMBAG,IDautobusa,IME,PREZIME,E_MAIL) values (5,36985482,1,'Petra','Jurić','pjuric@vub.hr');
Insert into putnici (ID,JMBAG,IDautobusa,IME,PREZIME,E_MAIL) values (6,36985483,2,'Lana','Novak','lnovak@vub.hr');
Insert into putnici (ID,JMBAG,IDautobusa,IME,PREZIME,E_MAIL) values (7,36985484,null,'Dora','Kovačić','dkovacic@vub.hr');
Insert into putnici (ID,JMBAG,IDautobusa,IME,PREZIME,E_MAIL) values (8,36985485,3,'Marta','Vuković','mvukovic@vub.hr');
Insert into putnici (ID,JMBAG,IDautobusa,IME,PREZIME,E_MAIL) values (9,36985486,2,'Luka','Knežević','lknezevuc@vub.hr');
Insert into putnici (ID,JMBAG,IDautobusa,IME,PREZIME,E_MAIL) values (10,36985487,2,'Marko','Marković','mmarkovic@vub.hr');
Insert into putnici (ID,JMBAG,IDautobusa,IME,PREZIME,E_MAIL) values (11,36985488,null,'Jakov','Petrović','jpetrovic@vub.hr');
Insert into putnici (ID,JMBAG,IDautobusa,IME,PREZIME,E_MAIL) values (12,36985489,2,'Ivan','Matić','imatic@vub.hr');
Insert into putnici (ID,JMBAG,IDautobusa,IME,PREZIME,E_MAIL) values (13,36985490,3,'Petar','Tomić','ptomic@vub.hr');
Insert into putnici (ID,JMBAG,IDautobusa,IME,PREZIME,E_MAIL) values (14,36985491,null,'Matej','Kovač','mkovac@vub.hr');
Insert into putnici (ID,JMBAG,IDautobusa,IME,PREZIME,E_MAIL) values (15,36985492,3,'Filip','Pavlović','fpavlovic@vub.hr');
Insert into putnici (ID,JMBAG,IDautobusa,IME,PREZIME,E_MAIL) values (16,36985493,null,'Mario','Perić','mperic@vub.hr');
Insert into putnici (ID,JMBAG,IDautobusa,IME,PREZIME,E_MAIL) values (17,36985494,null,'Anita','Jurković','ajurkovic@vub.hr');
Insert into putnici (ID,JMBAG,IDautobusa,IME,PREZIME,E_MAIL) values (18,36985495,null,'Kazimir','Gelić','kgelic@vub.hr');
Insert into putnici (ID,JMBAG,IDautobusa,IME,PREZIME,E_MAIL) values (19,36985496,null,'Borna','Špiranec','spiranec@vub.hr');
Insert into putnici (ID,JMBAG,IDautobusa,IME,PREZIME,E_MAIL) values (20,36985497,null,'Vesna','Molnar','vmolnar@vub.hr');
Insert into putnici (ID,JMBAG,IDautobusa,IME,PREZIME,E_MAIL) values (21,36985498,2,'Marija','Horvat','mhorvat1@vub.hr');
Insert into putnici (ID,JMBAG,IDautobusa,IME,PREZIME,E_MAIL) values (22,36985499,2,'Miroslav','Horvat','mhorvat2@vub.hr');
Insert into putnici (ID,JMBAG,IDautobusa,IME,PREZIME,E_MAIL) values (23,36985500,3,'Berislav','Marić','bmaric@vub.hr');
Insert into putnici (ID,JMBAG,IDautobusa,IME,PREZIME,E_MAIL) values (24,36985501,1,'Jakov','Kovačić','jkovacic@vub.hr');
Insert into putnici (ID,JMBAG,IDautobusa,IME,PREZIME,E_MAIL) values (25,36985502,1,'Filip','Kovačić','fkovacic@vub.hr');
Insert into putnici (ID,JMBAG,IDautobusa,IME,PREZIME,E_MAIL) values (26,36985503,1,'Filip','Novak','fnovak@vub.hr');