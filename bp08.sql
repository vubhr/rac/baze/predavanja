--1 koristeći decode naredbu ispišite imena i prezimena korisnika iz tablice common.korisnici i spišite spol
select
   ime, prezime, decode(spol,
                        1, 'žensko',
                        0, 'muško',
                        'nepoznato') as spol
from
   common.korisnici

--2 Koliko ima broja 1 u broj 123451
SELECT 
   REGEXP_COUNT ('123451 ', '1') broj_znakova_1
FROM 
   dual;

--3 dohvatite mjesta iz tablice common.naselja i ispišite od koliko se riječi satoji mjesto
select 
  decode (REGEXP_COUNT (rtrim(mjesto), ' '),
          0, 'jedna riječ',
          1, 'dvije riječi',
          2, 'tri riječi',
          3, 'četiri riječi',
          'čudno') broj_riječi,
  mjesto
from 
  common.naselja

 


--4 dohvaćanje broja zapisa iz tablica
select count(1) from common.kontakti
select count(1) from common.korisnici
--5 dohvaćanje korisnika koji nemaju kontakte
select 
   kor.id, 
   kor.ime, 
   kor.prezime, 
  (select 
     kon.id 
   from 
      common.kontakti kon 
   where 
      kor.id = kon.idkorisnika) as idkontakta
from 
   common.korisnici kor
--6 dohvaćanje korisnika koji nemaju kontakte u vanjski SELECT
select * from (
   select 
      kor.id, 
      kor.ime, 
      kor.prezime, 
     (select 
        kon.id 
      from 
         common.kontakti kon 
      where 
         kor.id = kon.idkorisnika) as idkontakta
   from 
      common.korisnici kor)
--7 uvjet u vanjskom selectu na prethodni zadatak
select * from (
   select 
      kor.id, 
      kor.ime, 
      kor.prezime, 
     (select 
        kon.id 
      from 
         common.kontakti kon 
      where 
         kor.id = kon.idkorisnika) as idkontakta
   from 
      common.korisnici kor)
   where idkontakta = null   
      
--8 dohvaćanje svih korisnika koji imaju kontakte pomoću exists operatora
select 
  *
from 
   common.korisnici kor
where exists
   (select 
      * 
    from 
       common.kontakti kon 
    where 
       kor.id = kon.idkorisnika)
--9 dohvaćanje svih korisnika koji nemaju kontakte pomoću not exists operatora
select 
  *
from 
   common.korisnici kor
where not exists
   (select 
      * 
    from 
       common.kontakti kon 
    where 
       kor.id = kon.idkorisnika)
--10 dohvaćanje kontakata koji imaju korisnike
select 
  * 
from 
  common.kontakti 
where idkorisnika in (
   select 
      id
   from 
      common.korisnici kor
   where exists
      (select 
         * 
       from 
          common.kontakti kon 
       where 
          kor.id = kon.idkorisnika)
)
--11 dohvaćanje kontakata koji nemaju korisnike
select 
  * 
from 
  common.kontakti 
where idkorisnika in (
   select 
      id
   from 
      common.korisnici kor
   where not exists
      (select 
         * 
       from 
          common.kontakti kon 
       where 
          kor.id = kon.idkorisnika)
)

--12 koristeći inner join spojite tablice common.korisnici i common.kontakti
select 
  * 
from 
  common.korisnici kor 
inner join
  common.kontakti kon
on
  kor.id = kon.idkorisnika
--13 koristeći inner join spojite tablice common.korisnici, common.kontakti i common.naselja
select 
  * 
from 
  common.korisnici kor 
inner join
  common.kontakti kon
on
  kor.id = kon.idkorisnika
inner join
  common.naselja n
on 
  n.id = kon.idnaselja
--14 koristeći inner join spojite tablice common.korisnici, common.kontakti, common.naselja i common zupanije
select 
   * 
from 
   common.korisnici kor 
inner join
   common.kontakti kon
on
   kor.id = kon.idkorisnika
inner join
   common.naselja n
on 
   n.id = kon.idnaselja
inner join 
   common.zupanije z
on
   n.idzupanija = z.id
   
--15 koristeći inner join spojite tablice common.korisnici, common.kontakti, common.naselja i common zupanije
-- i dohvatite sve korisnike iz BJELOVARSKO-BOLOGORSKE županije
select 
   * 
from 
   common.korisnici kor 
inner join
   common.kontakti kon
on
   kor.id = kon.idkorisnika
inner join
   common.naselja n
on 
   n.id = kon.idnaselja
inner join 
   common.zupanije z
on
   n.idzupanija = z.id
where
   z.zupanija = 'BJELOVARSKO-BILOGORSKA'
--16 dohvatite sve zapise iz tablice common.korisnici i sve podatke koji se mogu povezati u common.kontaktima
select 
   * 
from 
   common.korisnici kor 
left join
   common.kontakti kon
on
   kor.id = kon.idkorisnika
where 
   kon.idkorisnika is null
   
--17 izmijenite prethodni upit tako što ćete zamijeniti LEFT JOIN s RIGHT JOINom
select 
   * 
from 
   common.korisnici kor 
right join
   common.kontakti kon
on
   kor.id = kon.idkorisnika
where 
   kon.idkorisnika is null
--18 Zamijenite redoslijed tablica u prethodnom upitu
select 
   * 
from 
   common.kontakti kon
right join
   common.korisnici kor 
on
   kor.id = kon.idkorisnika
where 
   kon.idkorisnika is null
--19 spojite common.korisnici i common.kontakti u full JOIN 
select 
   * 
from 
   common.korisnici kor
full join
   common.kontakti kon 
on
   kor.id = kon.idkorisnika

--20 spojite common.korisnici i common.kontakti u full JOIN i izbacite se korisnike koji nemaju kontakte
select 
   * 
from 
   common.korisnici kor
full join
   common.kontakti kon
on
   kor.id = kon.idkorisnika
where 
   kon.idkorisnika is null




