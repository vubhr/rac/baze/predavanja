--0
--------------------------------------------------------------------------
set serveroutput on
declare
  in_json varchar(1000) :='{"ime":"Marko"}';
begin
   print(in_json);
end;


--1
---------------------------------------------------------------------------
set serveroutput on
declare
  in_json varchar(1000) :='{"ime":"Marko"}';
  l_tmp varchar2(1000);
begin
   select
      JSON_VALUE(in_json, '$.ime') 
   into 
      l_tmp
   FROM 
     dual;
   
   print(l_tmp);  
end;

--2
---------------------------------------------------------------------------
set serveroutput on
declare
  in_json varchar(1000) :='{"ime":"Marko", "prezime":"Marković"}';
  l_tmp1 varchar2(1000);
  l_tmp2 varchar2(1000);
begin   
   select
      JSON_VALUE(in_json, '$.ime'),
      JSON_VALUE(in_json, '$.prezime')
   into 
      l_tmp1,
      l_tmp2
   FROM 
     dual;
   
   print(l_tmp1 || ' ' || l_tmp2);  
end;
--3
----------------------------------------------------------------------------
 SELECT 
   json_object('ID' VALUE ID, 
               'ZUPANIJA' VALUE ZUPANIJA) as izlaz
 FROM
    zupanije;

--4
---------------------------------------------------------------------------
set serveroutput on
declare
  in_json varchar(1000) :='{"ime":"Marko", "prezime":"Marković", kontakt:{"email":"mmarkovic@vub.hr"}}';
  l_tmp1 varchar2(1000);
  l_tmp2 varchar2(1000);
  l_tmp3 varchar2(1000);
begin 
   select
      JSON_VALUE(in_json, '$.ime'),
      JSON_VALUE(in_json, '$.prezime'),
      JSON_VALUE(in_json, '$.kontakt')
   into 
      l_tmp1,
      l_tmp2,
      l_tmp3
   FROM 
     dual;
   
   print(l_tmp1 || ' ' || l_tmp2 || ' ' || l_tmp3);  
end;

--5
---------------------------------------------------------------------------
set serveroutput on
declare
  in_json varchar(1000) :='{"ime":"Marko", "prezime":"Marković", kontakt:{"email":"mmarkovic@vub.hr", "mobitel":09812345868}}';
  l_tmp1 varchar2(1000);
  l_tmp2 varchar2(1000);
  l_tmp3 varchar2(1000);
begin  
   select
      JSON_VALUE(in_json, '$.ime'),
      JSON_VALUE(in_json, '$.prezime'),
      JSON_VALUE(in_json, '$.kontakt.email')
   into 
      l_tmp1,
      l_tmp2,
      l_tmp3
   FROM 
     dual;
   
   print(l_tmp1 || ' ' || l_tmp2 || ' ' || l_tmp3);  
end;

--6
---------------------------------------------------------------------------
set serveroutput on
declare
  in_json varchar(1000) :='{"ime":"Marko", "prezime":"Marković", kontakt:{"email":"mmarkovic@vub.hr","mobitel":09812345868}}';
  l_tmp1 varchar2(1000);
begin
   select
      JSON_QUERY(in_json, '$.kontakt')
   into 
      l_tmp1
   FROM 
     dual;     
   
   print('kontakt json = ' || l_tmp1);  
end;

--7
---------------------------------------------------------------------------
set serveroutput on
declare
  in_json varchar(1000) :='{"ime":"Marko", "prezime":"Marković", kontakt:{"email":"mmarkovic@vub.hr","mobitel":09812345868}}';
  l_tmp1 varchar2(1000);
  l_tmp2 varchar2(1000);
  l_tmp3 varchar2(1000);
begin
   select
      JSON_QUERY(in_json, '$.kontakt')
   into 
      l_tmp3
   FROM 
     dual;     
  
  select
      JSON_VALUE(l_tmp3, '$.email'),
      JSON_VALUE(l_tmp3, '$.mobitel')
   into 
      l_tmp1,
      l_tmp2
   FROM 
     dual;   
     
   print(l_tmp1 || ' ' || l_tmp2);  
end;

--8
---------------------------------------------------------------------------
set serveroutput on
declare
  in_json varchar(1000) :='[{"ime":"Marko", "prezime":"Marković"}, {"ime":"Petar", "prezime":"Petrović"}]';
  l_tmp1 varchar2(1000);
  l_tmp2 varchar2(1000);
  l_tmp3 varchar2(1000);
  l_tmp4 varchar2(1000);
begin  
   select
      JSON_QUERY(in_json, '$[0,1]' with wrapper)
   into 
      l_tmp4
   FROM 
     dual;    
     
   select
      JSON_QUERY(l_tmp4, '$[0]'),
      JSON_QUERY(l_tmp4, '$[1]')
   into 
      l_tmp1,
      l_tmp2
   FROM 
     dual;       
     
   print(l_tmp1 || ' ' || l_tmp2 || ' cijeli json = ' || l_tmp4);  
end;

--9
---------------------------------------------------------------------------
set serveroutput on
declare
  in_json varchar(1000) :='[{"ime":"Marko", "prezime":"Marković"}, {"ime":"Petar", "prezime":"Petrović"}]';
  l_tmp1 varchar2(1000);
  l_tmp2 varchar2(1000);
  l_tmp3 varchar2(1000);
  l_tmp4 varchar2(1000);
begin
   print('input:' || in_json);
  
   select
      JSON_QUERY(in_json, '$[*]' with wrapper)
   into 
      l_tmp4
   FROM 
     dual;    
     
   select
      JSON_QUERY(l_tmp4, '$[0]'),
      JSON_QUERY(l_tmp4, '$[1]')
   into 
      l_tmp1,
      l_tmp2
   FROM 
     dual;       

   select
      JSON_VALUE(l_tmp1, '$.ime')
   into 
      l_tmp3
   FROM 
     dual;       
    
   print(l_tmp3);  
end;

--10
----------------------------------------------------------------------------------------------
set serveroutput on
declare
  in_json varchar(1000) :='[{"ime":"Marko", "prezime":"Marković"}, {"ime":"Petar", "prezime":"Petrović"}]';
  l_tmp1 varchar2(1000);
  l_tmp2 varchar2(1000);
  l_tmp3 varchar2(1000);
  l_tmp4 varchar2(1000);
begin
   print(in_json);
  
   select
      JSON_dataguide(in_json)
   into 
      l_tmp4
   FROM 
     dual;    
     
   print(l_tmp4);  
end;

--11
-----------------------------------------------------------------------------------------------
set serveroutput on
declare
  in_json varchar(1000) :='[{"ime":"Marko", "prezime":"Marković"}, {"ime":"Petar", "prezime":"Petrović"}]';
  l_tmp1 varchar2(1000);
  l_tmp2 varchar2(1000);
  l_tmp3 varchar2(1000);
  l_tmp4 varchar2(1000);
begin
   print(in_json);
  
   select
      JSON_dataguide(in_json, dbms_json.FORMAT_FLAT, dbms_json.pretty)
   into 
      l_tmp4
   FROM 
     dual;    
     
   print(l_tmp4);  
end;

--12
---------------------------------------------------------------------------------------------------------
SELECT *
FROM JSON_TABLE('[{"ime":"Marko", "prezime":"Marković"}, {"ime":"Petar", "prezime":"Petrović"}]', '$'
COLUMNS (Marko VARCHAR2(40) FORMAT JSON PATH '$[0]',
         Petar VARCHAR2(40) FORMAT JSON PATH '$[1]'));
         
--13
---------------------------------------------------------------------------------------------------------        
SELECT *
FROM JSON_TABLE('[{"ime":"Marko", "prezime":"Marković"}, {"ime":"Petar", "prezime":"Petrović"}]', '$[*]'
COLUMNS
  (RBR FOR ORDINALITY,
   ime VARCHAR2(100) PATH '$.ime',
   prezime VARCHAR2(100) PATH '$.prezime')); 
   
--14   
-------------------------------------------------------------------------------------------------------
SELECT *
FROM JSON_TABLE('{"ime":"Marko", "prezime":"Marković", kontakt:{"email":"mmarkovic@vub.hr","mobitel":09812345868}}', '$.kontakt'
COLUMNS (email VARCHAR2(40) PATH '$.email',
         mobitel VARCHAR2(40) PATH '$.mobitel'));

--15
----------------------------------------------------------------------------------------------------------
SELECT *
FROM JSON_TABLE('{
  "zupanije": [
    {
      "naziv": "Zagrebačka županija",
      "povrsina": 3060,
      "sjediste": "Zagreb",
      "stanovnika": 317606,
      "slika": "EU13_0020_m.png",
      "link": "http://www.zagrebacka-zupanija.hr/"
    },
    {
      "naziv": "Krapinsko-zagorska županija",
      "povrsina": 1229,
      "sjediste": "Krapina",
      "stanovnika": 132892,
      "slika": "EU13_0007_m.png",
      "link": "http://www.kzz.hr/"
    },
    {
      "naziv": "Sisačko-moslavačka županija",
      "povrsina": 4468,
      "sjediste": "Sisak",
      "stanovnika": 172439,
      "slika": "EU13_0013_m.png",
      "link": "http://www.smz.hr/"
    },
    {
      "naziv": "Karlovačka županija",
      "povrsina": 3626,
      "sjediste": "Karlovac",
      "stanovnika": 128899,
      "slika": "EU13_0005_m.png",
      "link": "http://www.kazup.hr/"
    },
    {
      "naziv": "Varaždinska županija",
      "povrsina": 1262,
      "sjediste": "Varaždin",
      "stanovnika": 175951,
      "slika": "EU13_0016_m.png",
      "link": "http://www.varazdinska-zupanija.hr/"
    },
    {
      "naziv": "Koprivničko-križevačka županija",
      "povrsina": 1748,
      "sjediste": "Koprivnica",
      "stanovnika": 115584,
      "slika": "EU13_0006_m.png",
      "link": "http://www.kckzz.hr/"
    },
    {
      "naziv": "Bjelovarsko-bilogorska županija",
      "povrsina": 2640,
      "sjediste": "Bjelovar",
      "stanovnika": 119764,
      "slika": "EU13_0001_m.png",
      "link": "http://www.bbz.hr/"
    },
    {
      "naziv": "Primorsko-goranska županija",
      "povrsina": 3588,
      "sjediste": "Rijeka",
      "stanovnika": 296195,
      "slika": "EU13_0012_m.png",
      "link": "http://www.pgz.hr/"
    },
    {
      "naziv": "Ličko-senjska županija",
      "povrsina": 5353,
      "sjediste": "Gospić",
      "stanovnika": 50927,
      "slika": "EU13_0008_m.png",
      "link": "http://www.licko-senjska.hr/"
    },
    {
      "naziv": "Virovitičko-podravska županija",
      "povrsina": 2024,
      "sjediste": "Virovitica",
      "stanovnika": 84836,
      "slika": "EU13_0017_m.png",
      "link": "http://www.vpz.com.hr/"
    },
    {
      "naziv": "Požeško-slavonska županija",
      "povrsina": 1823,
      "sjediste": "Požega",
      "stanovnika": 78034,
      "slika": "EU13_0011_m.png",
      "link": "http://www.pszupanija.hr/"
    },
    {
      "naziv": "Brodsko-posavska županija",
      "povrsina": 2030,
      "sjediste": "Slavonski Brod",
      "stanovnika": 158575,
      "slika": "EU13_0002_m.png",
      "link": "http://www.bpz.hr/"
    },
    {
      "naziv": "Zadarska županija",
      "povrsina": 3646,
      "sjediste": "Zadar",
      "stanovnika": 170017,
      "slika": "EU13_0019_m.png",
      "link": "http://www.zadarska-zupanija.hr/"
    },
    {
      "naziv": "Osječko-baranjska županija",
      "povrsina": 4155,
      "sjediste": "Osijek",
      "stanovnika": 305032,
      "slika": "EU13_0010_m.png",
      "link": "http://www.obz.hr/"
    },
    {
      "naziv": "Šibensko-kninska županija",
      "povrsina": 2984,
      "sjediste": "Šibenik",
      "stanovnika": 109375,
      "slika": "EU13_0015_m.png",
      "link": "http://www.sibensko-kninska-zupanija.hr/"
    },
    {
      "naziv": "Vukovarsko-srijemska županija",
      "povrsina": 2454,
      "sjediste": "Vukovar",
      "stanovnika": 179521,
      "slika": "EU13_0018_m.png",
      "link": "http://www.vusz.hr/"
    },
    {
      "naziv": "Splitsko-dalmatinska županija",
      "povrsina": 4540,
      "sjediste": "Split",
      "stanovnika": 454798,
      "slika": "EU13_0014_m.png",
      "link": "http://www.dalmacija.hr/"
    },
    {
      "naziv": "Istarska županija",
      "povrsina": 2813,
      "sjediste": "Pazin",
      "stanovnika": 208055,
      "slika": "EU13_0004_m.png",
      "link": "http://www.istra-istria.hr/"
    },
    {
      "naziv": "Dubrovačko-neretvanska županija",
      "povrsina": 1781,
      "sjediste": "Dubrovnik",
      "stanovnika": 122568,
      "slika": "EU13_0003_m.png",
      "link": "http://edubrovnik.org/"
    },
    {
      "naziv": "Međimurska županija",
      "povrsina": 729,
      "sjediste": "Čakovec",
      "stanovnika": 113804,
      "slika": "EU13_0009_m.png",
      "link": "http://www.medjimurska-zupanija.hr/"
    },
    {
      "naziv": "Grad Zagreb",
      "povrsina": 641,
      "sjediste": "Grad Zagreb",
      "stanovnika": 790017,
      "slika": "EU13_0021_m.png",
      "link": "http://www.zagreb.hr/"
    }
  ]
}', '$.zupanije[*]'
COLUMNS (naziv VARCHAR2(100) PATH '$.naziv',
         sjediste VARCHAR2(100) PATH '$.sjediste',
         stanovnika number(8) PATH '$.stanovnika'));
--16
-------------------------------------------------------------------------------
declare
  l_naziv varchar2(1000);
  l_sjediste varchar2(1000);
  l_stanovnika varchar2(1000);
  l_clob clob := to_clob('{
  "zupanije": [
    {
      "naziv": "Zagrebačka županija",
      "povrsina": 3060,
      "sjediste": "Zagreb",
      "stanovnika": 317606,
      "slika": "EU13_0020_m.png",
      "link": "http://www.zagrebacka-zupanija.hr/"
    },
    {
      "naziv": "Krapinsko-zagorska županija",
      "povrsina": 1229,
      "sjediste": "Krapina",
      "stanovnika": 132892,
      "slika": "EU13_0007_m.png",
      "link": "http://www.kzz.hr/"
    },
    {
      "naziv": "Sisačko-moslavačka županija",
      "povrsina": 4468,
      "sjediste": "Sisak",
      "stanovnika": 172439,
      "slika": "EU13_0013_m.png",
      "link": "http://www.smz.hr/"
    },
    {
      "naziv": "Karlovačka županija",
      "povrsina": 3626,
      "sjediste": "Karlovac",
      "stanovnika": 128899,
      "slika": "EU13_0005_m.png",
      "link": "http://www.kazup.hr/"
    },
    {
      "naziv": "Varaždinska županija",
      "povrsina": 1262,
      "sjediste": "Varaždin",
      "stanovnika": 175951,
      "slika": "EU13_0016_m.png",
      "link": "http://www.varazdinska-zupanija.hr/"
    },
    {
      "naziv": "Koprivničko-križevačka županija",
      "povrsina": 1748,
      "sjediste": "Koprivnica",
      "stanovnika": 115584,
      "slika": "EU13_0006_m.png",
      "link": "http://www.kckzz.hr/"
    },
    {
      "naziv": "Bjelovarsko-bilogorska županija",
      "povrsina": 2640,
      "sjediste": "Bjelovar",
      "stanovnika": 119764,
      "slika": "EU13_0001_m.png",
      "link": "http://www.bbz.hr/"
    },
    {
      "naziv": "Primorsko-goranska županija",
      "povrsina": 3588,
      "sjediste": "Rijeka",
      "stanovnika": 296195,
      "slika": "EU13_0012_m.png",
      "link": "http://www.pgz.hr/"
    },
    {
      "naziv": "Ličko-senjska županija",
      "povrsina": 5353,
      "sjediste": "Gospić",
      "stanovnika": 50927,
      "slika": "EU13_0008_m.png",
      "link": "http://www.licko-senjska.hr/"
    },
    {
      "naziv": "Virovitičko-podravska županija",
      "povrsina": 2024,
      "sjediste": "Virovitica",
      "stanovnika": 84836,
      "slika": "EU13_0017_m.png",
      "link": "http://www.vpz.com.hr/"
    },
    {
      "naziv": "Požeško-slavonska županija",
      "povrsina": 1823,
      "sjediste": "Požega",
      "stanovnika": 78034,
      "slika": "EU13_0011_m.png",
      "link": "http://www.pszupanija.hr/"
    },
    {
      "naziv": "Brodsko-posavska županija",
      "povrsina": 2030,
      "sjediste": "Slavonski Brod",
      "stanovnika": 158575,
      "slika": "EU13_0002_m.png",
      "link": "http://www.bpz.hr/"
    },
    {
      "naziv": "Zadarska županija",
      "povrsina": 3646,
      "sjediste": "Zadar",
      "stanovnika": 170017,
      "slika": "EU13_0019_m.png",
      "link": "http://www.zadarska-zupanija.hr/"
    },
    {
      "naziv": "Osječko-baranjska županija",
      "povrsina": 4155,
      "sjediste": "Osijek",
      "stanovnika": 305032,
      "slika": "EU13_0010_m.png",
      "link": "http://www.obz.hr/"
    },
    {
      "naziv": "Šibensko-kninska županija",
      "povrsina": 2984,
      "sjediste": "Šibenik",
      "stanovnika": 109375,
      "slika": "EU13_0015_m.png",
      "link": "http://www.sibensko-kninska-zupanija.hr/"
    },
    {
      "naziv": "Vukovarsko-srijemska županija",
      "povrsina": 2454,
      "sjediste": "Vukovar",
      "stanovnika": 179521,
      "slika": "EU13_0018_m.png",
      "link": "http://www.vusz.hr/"
    },
    {
      "naziv": "Splitsko-dalmatinska županija",
      "povrsina": 4540,
      "sjediste": "Split",
      "stanovnika": 454798,
      "slika": "EU13_0014_m.png",
      "link": "http://www.dalmacija.hr/"
    },
    {
      "naziv": "Istarska županija",
      "povrsina": 2813,
      "sjediste": "Pazin",
      "stanovnika": 208055,
      "slika": "EU13_0004_m.png",
      "link": "http://www.istra-istria.hr/"
    },
    {
      "naziv": "Dubrovačko-neretvanska županija",
      "povrsina": 1781,
      "sjediste": "Dubrovnik",
      "stanovnika": 122568,
      "slika": "EU13_0003_m.png",
      "link": "http://edubrovnik.org/"
    },
    {
      "naziv": "Međimurska županija",
      "povrsina": 729,
      "sjediste": "Čakovec",
      "stanovnika": 113804,
      "slika": "EU13_0009_m.png",
      "link": "http://www.medjimurska-zupanija.hr/"
    },
    {
      "naziv": "Grad Zagreb",
      "povrsina": 641,
      "sjediste": "Grad Zagreb",
      "stanovnika": 790017,
      "slika": "EU13_0021_m.png",
      "link": "http://www.zagreb.hr/"
    }
  ]
}');
begin
  SELECT 
    naziv,
    sjediste,
    stanovnika
  into
    l_naziv,
    l_sjediste,
    l_stanovnika
    
  FROM JSON_TABLE(l_clob, '$.zupanije[20]'
  COLUMNS (naziv VARCHAR2(100) PATH '$.naziv',
         sjediste VARCHAR2(100) PATH '$.sjediste',
         stanovnika number(8) PATH '$.stanovnika'));
  
  for x in (
      SELECT 
        naziv,
        sjediste,
        stanovnika    
      FROM 
         JSON_TABLE(l_clob, '$.zupanije[*]'
            COLUMNS (naziv VARCHAR2(100) PATH '$.naziv',
                     sjediste VARCHAR2(100) PATH '$.sjediste',
                     stanovnika number(8) PATH '$.stanovnika'))
  )
  loop
     print(x.naziv || ' ' || x.sjediste || ' ' || x.stanovnika);   
  end loop;
  
end;


