--3
set serveroutput on
begin
   f_krug(1);
end;

--4
set serveroutput on
declare
   l_result number;
begin
   l_result := f_krug(1);
   print( 'Površina kruga radijusa 1cm je ' || l_result || 'cm2');
end;

--5
set serveroutput on
begin
   print( 'Površina kruga radijusa 1cm je ' || f_krug(1) || 'cm2');
   if (f_krug(1)) < 4 then
      print( 'Površina kruga radijusa 1cm je manja od 4 cm2');
   else
      print( 'Površina kruga radijusa 1cm je veća od 4 cm2');
   end if;
end;

--6
set serveroutput on
declare
 l_radijus number := 1.1;
 l_rezultat number;
begin
   l_rezultat := f_krug(l_radijus);
   print( 'Površina kruga radijusa ' || l_radijus || 'cm je ' || l_rezultat || 'cm2');
   if (l_rezultat) < 4 then
      print( 'Površina kruga radijusa ' || l_radijus || 'cm je manja od 4 cm2');
   else
      print( 'Površina kruga radijusa ' || l_radijus || 'cm je veća od 4 cm2');
   end if;
end;

--7
select f_krug(1) as rezultat from dual;
--8
Select 
  mjesto, 
  postanskibroj, 
  f_krug(1) as rezultat 
from 
  common.naselja;  
--9 
Select 
  mjesto, 
  postanskibroj / f_krug(1) as kvocijent
from 
  common.naselja;
--10
select 
   id,
   zupanija,
   p_naselja.f_naselja_br(id) as broj_naselja
from
   common.zupanije;
--11
select 
   id,
   zupanija,
   p_naselja.f_naselja_br(id) as broj_naselja
from
   common.zupanije
order by 
   p_naselja.f_naselja_br(id) desc;
--12

select 
   id,
   zupanija,
   p_naselja.f_naselja_br(id) as broj_naselja,
   case 
   when p_naselja.f_naselja_br(id) > 0 and p_naselja.f_naselja_br(id) < 200 then
     'mali broj naselja'
   when p_naselja.f_naselja_br(id) >= 200 and p_naselja.f_naselja_br(id) < 400 then  
     'srednji broj naselja'
   when p_naselja.f_naselja_br(id) >= 400 then
     'veliki broj naselja'
   end as veličina
from
   common.zupanije
order by 
   p_naselja.f_naselja_br(id) desc;
   
select 
  opcina, 
  p_naselja.f_opcine_br(ID) as broj_naselja 
from 
  common.naselja 
where 
  mjesto = opcina
order by 
  p_naselja.f_opcine_br(ID) desc

select opcina, count(1) as broj_naselja from common.naselja group by opcina
order by count(1) desc

select 
        count(1), 
        opcina
     
     from 
        common.naselja 
     --where
     --   id = 8
     group by 
        opcina;
BEGIN        
select router.p_main


  
