CREATE OR REPLACE PACKAGE BODY ROUTER AS

-------------------------------------------------------------
--p_main
  procedure p_main(p_in in clob, p_out out clob) AS
    l_obj         json_object_t;
    l_string      varchar2(4000);
    l_statement   VARCHAR2(120) := 'ALTER SESSION SET NLS_NUMERIC_CHARACTERS = ' ||  ''',.''';  
    l_procedura   VARCHAR2(40);
 BEGIN
    l_obj := json_object_t(p_in);
    l_string := l_obj.to_clob;
    l_procedura := JSON_VALUE(l_string, '$.procedura' RETURNING VARCHAR2);
    
    CASE l_procedura
       WHEN 'p_login' THEN
          dohvati.p_login(l_obj);
       WHEN 'p_zupanije' THEN
          dohvati.p_zupanije(l_obj);
       WHEN 'p_test' THEN
          dohvati.p_test(l_obj);
       ELSE
          l_obj.put('h_message', 'Nepoznata metoda ' || l_procedura);
          l_obj.put('h_errcod', 997);
    END CASE;

    p_out := l_obj.to_clob;
 EXCEPTION
    WHEN e_iznimka THEN
       p_out := l_obj.to_clob;
    WHEN OTHERS THEN
       DECLARE
          l_error PLS_INTEGER := SQLCODE;
          l_msg VARCHAR2(255) := sqlerrm;
       BEGIN
          CASE l_error    
          WHEN -2292 THEN
             l_obj.put('h_message', 'Navedeni zapis se ne može obrisati jer postoje veze na druge zapise');
             l_obj.put('h_errcode', 99);
          ELSE
             common.p_errlog('p_main', dbms_utility.format_error_backtrace, sqlcode, sqlerrm, l_string);
             l_obj.put('h_message', 'Greška u obradi podataka');
             l_obj.put('h_errcode', 100);
             ROLLBACK;
          END CASE;
       END;
       p_out := l_obj.to_clob;
  END p_main;

END ROUTER;