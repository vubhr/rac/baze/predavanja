CREATE OR REPLACE PACKAGE BODY DOHVATI AS
 
-------------------------------------------------------
--p_test
  procedure p_test(l_obj in out JSON_OBJECT_T) AS
  l_string varchar2(4000);  
  BEGIN
   l_string := l_obj.TO_STRING; 
   common.p_logiraj('tu sam', 'evo ga tu je');
   l_obj.put('pozdrav', 'Hello World!');
   common.p_logiraj('tu sam', 'evo ga tu je 2');
  END p_test;
------------------------------------------------------------
--p_login
  procedure p_login(l_obj in out JSON_OBJECT_T) AS
  l_string varchar2(4000);  
   l_username    common.korisnici.email%type;
   l_password    common.korisnici.password%type;
   l_id          common.korisnici.id%type;
   l_record      VARCHAR2(4000);
   l_out         json_array_t := json_array_t('[]');
BEGIN
   l_string := l_obj.TO_STRING; 
   l_username := JSON_VALUE(l_string, '$.username' RETURNING VARCHAR2);
   l_password := JSON_VALUE(l_string, '$.password' RETURNING VARCHAR2);

   IF (l_username IS NULL OR l_password is NULL) THEN
      l_obj.put('h_message', 'Molimo unesite korisničko ime i zaporku');
      l_obj.put('h_errcod', 101);
      RAISE e_iznimka;
   ELSE   
      SELECT 
         COUNT(1)
      INTO 
         l_id
      FROM 
         common.korisnici
      WHERE 
         email = l_username AND 
         password = l_password;
            

      IF l_id = 0 THEN
         l_obj.put('h_message', 'Nepoznato korisničko ime ili zaporka');
         l_obj.put('h_errcode', 96);
         RAISE e_iznimka;
      END IF;
         
      IF l_id > 1 THEN
         l_obj.put('h_message', 'Molimo javite se u Informatiku');
         l_obj.put('h_errcode', 42);
         RAISE e_iznimka;
      END IF;
                            
      SELECT
         JSON_OBJECT( 
            'ID' VALUE kor.id, 
            'ime' VALUE kor.ime, 
            'prezime' VALUE kor.prezime, 
            'OIB' VALUE kor.oib, 'email' VALUE kor.email, 
            'spol' VALUE kor.spol, 
            'ovlasti' VALUE kor.ovlasti)
      INTO 
         l_record
      FROM
          common.korisnici kor
      where 
          email = l_username AND 
          password = l_password;
          
    
      l_out.append(json_object_t(l_record));
      l_obj.put('data', l_out);
   END IF; 
EXCEPTION
   when e_iznimka then
      raise;
   when OTHERS THEN
      common.p_errlog('p_main', dbms_utility.format_error_backtrace, sqlcode, sqlerrm, l_string);
      l_obj.put('h_message', 'Greška u obradi podataka');
      l_obj.put('h_errcode', 91);
      raise; 
END p_login;
---------------------------------------------------------
--p_zupanije
  procedure p_zupanije(l_obj in out JSON_OBJECT_T) AS
   l_string varchar2(4000);
   l_out    json_array_t := json_array_t('[]');
   l_slova       varchar2(20) := 'SA';
BEGIN
   l_string := l_obj.to_string;
   --l_slova := JSON_VALUE(l_string, '$.slova' RETURNING VARCHAR2);
   FOR x IN (
       SELECT 
           json_object('ID' VALUE ID, 
                       'ZUPANIJA' VALUE ZUPANIJA) as izlaz
       FROM
          common.zupanije
       where 
         zupanija like '%' || l_slova || '%')
   LOOP 
      l_out.append(JSON_OBJECT_T(x.izlaz));
   END LOOP;
   l_obj.put('data', l_out);  
EXCEPTION
   when OTHERS THEN
      common.p_errlog('p_main', dbms_utility.format_error_backtrace, sqlcode, sqlerrm, l_string);
      l_obj.put('h_message', 'Greška u obradi podataka');
      l_obj.put('h_errcode', 91);
      raise; 
END p_zupanije;

END DOHVATI;