--0
select * from autobusi;
select * from putnici;
select 
   * 
from 
   autobusi a, 
   putnici p 
where 
   a.id = p.idautobusa;
--1
set serveroutput on
declare
   l_id autobusi.id%type;
   l_reg_oznaka autobusi.reg_oznaka%type;
   l_cijena autobusi.cijena%type;
begin
   select
      ID,
      REG_OZNAKA,
      CIJENA
   into
      l_id,
      l_reg_oznaka,
      l_cijena
   from
      autobusi
   where
      id = 1;
   if SQL%FOUND THEN
     print('Broj redaka = ' || SQL%ROWCOUNT);
     print('l_id = ' || l_id || ' ' ||
           'l_reg_oznaka ' || l_reg_oznaka || ' ' ||
           'l_cijena ' || l_cijena);
   end if;
end;
--2
set serveroutput on
BEGIN
   DELETE   FROM
      autobusi
  WHERE 
     id = 5;
   IF SQL%FOUND THEN
      DBMS_OUTPUT.PUT_LINE(SQL%ROWCOUNT);
   END IF
END;
--3
set serveroutput on
BEGIN
   UPDATE 
      autobusi
  SET 
      cijena = 299.99
  WHERE 
     id = 1;
   IF SQL%FOUND THEN
      print('Broj izmjena ' || SQL%ROWCOUNT);
   END IF;
END;
--4
set serveroutput on
declare
   l_id autobusi.id%type;
   l_reg_oznaka autobusi.reg_oznaka%type;
   l_cijena autobusi.cijena%type;
begin
   select
      ID,
      REG_OZNAKA,
      CIJENA
   into
      l_id,
      l_reg_oznaka,
      l_cijena
   from
      autobusi;
   if SQL%FOUND THEN
     print('Broj redaka = ' || SQL%ROWCOUNT);
     print('l_id = ' || l_id || ' ' ||
           'l_reg_oznaka ' || l_reg_oznaka || ' ' ||
           'l_cijena ' || l_cijena);
   end if;
end;
--5
set serveroutput on
begin
  update autobusi
  set cijena = cijena + 1;
  if SQL%FOUND then
     print('Izmjena na: ' || SQL%ROWCOUNT || ' retka');
  end if;
end;
--6
BEGIN
   UPDATE autobusi
   SET CIJENA = CIJENA - 1;
   COMMIT;
EXCEPTION
WHEN OTHERS THEN
   ROLLBACK;
END;
--7
set serveroutput on
DECLARE
  l_id autobusi.id%type;
  l_reg_oznaka autobusi.reg_oznaka%type;
  l_cijena autobusi.cijena%type;
  
  CURSOR cur_get_autobusi IS
  SELECT ID, REG_OZNAKA, CIJENA
  FROM AUTOBUSI WHERE ID = 1;
BEGIN
   OPEN cur_get_autobusi;
   FETCH cur_get_autobusi 
     INTO
         l_id,
         l_reg_oznaka,
         l_cijena;
    print('l_id= ' || l_id || ' ' ||
          'l_reg_oznaka= ' || l_reg_oznaka || ' ' ||
          'l_cijena= ' || l_cijena);     
    CLOSE cur_get_autobusi;     
END;
--8
set serveroutput on
DECLARE  
  CURSOR cur_get_putnici IS
  SELECT 
     ime,
     prezime
  FROM 
     putnici;
  l_putnici cur_get_putnici%rowtype;   
BEGIN
   OPEN cur_get_putnici;
   LOOP
      FETCH 
         cur_get_putnici 
      INTO 
         l_putnici;
      EXIT WHEN 
         cur_get_putnici%NOTFOUND;
      print(l_putnici.ime || ' ' || l_putnici.prezime);     
   END LOOP;
    
   IF cur_get_putnici%ISOPEN THEN
      CLOSE cur_get_putnici;
   END IF;
END;
--9
set serveroutput on
DECLARE
  l_reg_oznaka autobusi.reg_oznaka%type;
  l_cijena autobusi.cijena%type;
  
  CURSOR cur_get_autobusi IS
  SELECT 
    REG_OZNAKA, 
    CIJENA as stara,
    CIJENA * 1.1 as poskupljenje
  FROM 
    AUTOBUSI;
  l_autobusi cur_get_autobusi%rowtype;  
BEGIN
   print('Oznaka    Nova    Stara');
   OPEN cur_get_autobusi;
   LOOP
     FETCH 
        cur_get_autobusi 
     INTO
        l_autobusi;
     EXIT WHEN cur_get_autobusi%NOTFOUND;
     print(l_autobusi.reg_oznaka || ' ' || 
           l_autobusi.poskupljenje || ' ' ||
           l_autobusi.stara);
   END LOOP;  
   CLOSE cur_get_autobusi;     
END;
--10
set serveroutput on
DECLARE
  l_reg_oznaka autobusi.reg_oznaka%type;
  l_cijena autobusi.cijena%type;
  
  CURSOR cur_get_autobusi IS
  SELECT
    ID,
    CIJENA
  FROM 
    AUTOBUSI;
  l_autobusi cur_get_autobusi%rowtype;  
BEGIN
   OPEN cur_get_autobusi;
   LOOP
     FETCH 
        cur_get_autobusi 
     INTO
        l_autobusi;
     if (l_autobusi.cijena < 200) then
        update
          autobusi
        set cijena = 200
        where 
          id = l_autobusi.id;
     end if;
     EXIT WHEN cur_get_autobusi%NOTFOUND;
   END LOOP;  
   commit;
   CLOSE cur_get_autobusi;     
END;
--11
set serveroutput on
DECLARE
  CURSOR cur_get_putnici(l_koliko number DEFAULT 5) IS
  SELECT 
     ime,
     prezime
  FROM
     putnici 
  WHERE ID <= l_koliko;
  l_putnici cur_get_putnici%rowtype;   
BEGIN
   OPEN cur_get_putnici(7);
   LOOP
      FETCH 
         cur_get_putnici 
      INTO 
         l_putnici;
      EXIT WHEN 
         cur_get_putnici%NOTFOUND;
      print(l_putnici.ime || ' ' || l_putnici.prezime);     
   END LOOP;
    
   IF cur_get_putnici%ISOPEN THEN
      CLOSE cur_get_putnici;
   END IF;
end;

--12
set serveroutput on
DECLARE
  CURSOR cur_get_autobusi IS
  SELECT 
     id,
     cijena
  FROM
     autobusi;
BEGIN
   FOR l_autobusi IN cur_get_autobusi 
   LOOP
     print(L_AUTOBUSI.CIJENA);   
   END LOOP;
END;
     
--13
set serveroutput on
DECLARE
  l_putnici putnici%rowtype;
BEGIN
   FOR l_putnici IN 
      (SELECT
         ime,
         prezime
       FROM
         putnici p, autobusi a
       WHERE
         a.id = p.idautobusa and 
         a.id = 2
      )
   LOOP
     print(l_putnici.ime || ' ' || l_putnici.prezime);   
   END LOOP;
END;
     
--14
set serveroutput on
DECLARE
  CURSOR cur_get_autobusi IS
  SELECT 
    ID 
  FROM 
     autobusi;
  
  CURSOR cur_get_putnici(i_autobusi NUMBER) IS
  SELECT
    ime,
    prezime
  FROM
    putnici
  WHERE
    idautobusa = i_autobusi and rownum < 3;
BEGIN
   FOR l_autobusi IN 
      cur_get_autobusi 
   LOOP
      print(l_autobusi.id || '. autobus');
      FOR l_putnici IN 
         cur_get_putnici(l_autobusi.id)
      LOOP
         print(l_putnici.ime || ' ' || l_putnici.prezime);
      END LOOP;
      print(null);
   END LOOP;
END;
--15
update putnici set idautobusa = 3 where id = 8;

update putnici set idautobusa = 2 where id = 14;

--16
create or replace NONEDITIONABLE FUNCTION 
   F_CHANGE_BUS(I_ID1 IN NUMBER, I_ID2 IN NUMBER, O_message out varchar2) RETURN BOOLEAN 
IS
   L_AUTOBUS_ID1 PUTNICI.IDAUTOBUSA%TYPE;
   L_AUTOBUS_ID2 PUTNICI.IDAUTOBUSA%TYPE;
BEGIN
   if nvl(I_ID1, 0) = 0 then
      o_message := 'Molimo unesite prvog putnika';
      return false;
   end if;
   
   if nvl(I_ID2, 0) = 0 then
      o_message := 'Molimo unesite drugog putnika';
      return false;
   end if;
   
   begin 
       SELECT 
          IDAUTOBUSA
       INTO 
          L_AUTOBUS_ID1
       FROM 
          PUTNICI
       WHERE
          ID = I_ID1;
   exception
      when no_data_found then
         o_message := 'Ne postoji putnik s ID-em ' || I_ID1;
         return false;
   end;    
   
   begin
       SELECT 
          IDAUTOBUSA
       INTO 
          L_AUTOBUS_ID2
       FROM 
          PUTNICI
       WHERE
          ID = I_ID2;
   exception
      when no_data_found then
         o_message := 'Ne postoji putnik s ID-em ' || I_ID2;
         return false;
   end;       
   
   UPDATE 
      PUTNICI
   SET 
      IDAUTOBUSA = L_AUTOBUS_ID1
   WHERE 
     ID = I_ID2;

   UPDATE 
      PUTNICI
   SET 
      IDAUTOBUSA = L_AUTOBUS_ID2
   WHERE 
      ID = I_ID1;
   COMMIT;
   RETURN TRUE;
EXCEPTION
   WHEN OTHERS THEN
      o_message := 'Dogodila se greška u obradi podatka';
      ROLLBACK;
      RETURN FALSE;
END F_CHANGE_BUS;

--17
set serveroutput on
declare
o_message varchar2(300);
begin
  if f_change_bus(6,15, o_message) then
     print('Prošao update');
  else
     print(o_message);
  end if;
end;
     



