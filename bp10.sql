--0
select * from colors order by id desc;

INSERT INTO colors (SIFRA, NAZIV, COLCOD, RED, GREEN, BLUE, UPDATED, CREATED) 
VALUES ('bijela', 'bijela', '#FFF', 255, 255, 255,SYSTIMESTAMP, SYSTIMESTAMP);

INSERT INTO colors (SIFRA, NAZIV, COLCOD, RED, GREEN, BLUE) 
VALUES ('malo manje bijela', 'malo manje bijela', 'fffff0', 255, 255, 240);

INSERT INTO colors (SIFRA, NAZIV, COLCOD, RED, GREEN, BLUE) 
VALUES ('malo manje bijela', 'malo manje bijela', 'fffff0', 255, 255, 240);


--1
update colors set sifra = 'manje bijela' where id = 884;
select * from colors order by id desc

--2
BEGIN
   null;
END;

--3
SET SERVEROUTPUT ON
BEGIN
   dbms_output.put_line('Hello World!');
END;

--4
SET SERVEROUTPUT ON
DECLARE
   l_number1 number := 8;
   l_number2 number := 15;
   l_suma number;
BEGIN
   l_suma := l_number1 + l_number2;
   dbms_output.put_line('Suma brojeva ' || 
                        l_number1 || ' i ' || 
                        l_number2 || ' je ' ||
                        l_suma);
END;

--5
SET SERVEROUTPUT ON
DECLARE
   l_number1 number := 8;
   l_number2 number := 0;
   l_kvocijent number;
BEGIN
   l_kvocijent := l_number1/l_number2;
   dbms_output.put_line('Kvocijent brojeva ' || 
                        l_number1 || ' i ' || 
                        l_number2 || ' je ' ||
                        l_kvocijent);
EXCEPTION
   WHEN ZERO_DIVIDE THEN
      DBMS_OUTPUT.PUT_LINE('Dijeljenje s 0 nije dopuštena operacija');
   WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('Dogodila se greška koju nismo predvidjeli');
END;

--6
SET SERVEROUTPUT ON
DECLARE
   l_number1 number := 8;
   l_number2 number := 0;
   l_kvocijent number;
BEGIN
   l_kvocijent := l_number1/l_number2;
   dbms_output.put_line('Kvocijent brojeva ' || 
                        l_number1 || ' i ' || 
                        l_number2 || ' je ' ||
                        l_kvocijent);
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE = -1476 THEN
         DBMS_OUTPUT.PUT_LINE('Dijeljenje s 0 nije dopuštena operacija');
      ELSE
         DBMS_OUTPUT.PUT_LINE('Dogodila se greška koju nismo predvidjeli');
      END IF;
END;

--7
SET SERVEROUTPUT ON
DECLARE
   c_pi constant number default 3.14159;
   l_povrsina number;
   l_radijus number;
BEGIN
   l_povrsina := power(l_radijus,2)*c_pi;
   dbms_output.put_line('Površina kruga s radijusom ' || 
                        l_radijus || ' je ' || 
                        l_povrsina);
EXCEPTION
   WHEN OTHERS THEN 
      DBMS_OUTPUT.PUT_LINE('Dogodila se greška koju nismo predvidjeli');
END;

--8
DECLARE
  l_number pls_integer := 15.45;
BEGIN
   DBMS_OUTPUT.PUT_LINE( 'Broj ' || l_number);
END;



















