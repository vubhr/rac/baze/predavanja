--primjer1 read only tablica
alter table studenti read only;
select * from studenti;
update studenti set pozmob = 91 where id = 1;

--primjer2 vraćanje tablice iz read only status
alter table studenti read write;
select * from studenti;
update studenti set pozmob = 91 where id = 1;

--primjer3 izmjena naziva tablice
rename studenti to vubstudenti;
select * from studenti

--primjer4 izmjena naziva polja
select * from vubstudenti;
alter table vubstudenti rename column email to e_mail;

--primjer5 dodavanje polja
ALTER TABLE vubstudenti ADD( 
   deleted number(1,0) default 0 NOT NULL
);
select * from vubstudenti

--primjer 6 dodavanje invisible polja
ALTER TABLE vubstudenti ADD( 
   godina number(1,0) INVISIBLE default 1 NOT NULL 
);
select * from vubstudenti;
select v.*, godina from vubstudenti v

--primjer 7 fizičko brisanje polja
ALTER TABLE vubstudenti DROP COLUMN godina;
select v.*, godina from vubstudenti v;

--primjer 8 postavljanje polja na unused i brisanje unused polja
select * from vubstudenti
ALTER TABLE vubstudenti SET UNUSED (deleted);
select v.*, deleted from vubstudenti v
select * from vubstudenti

ALTER TABLE vubstudenti DROP UNUSED COLUMNS;

--primjer 9 izmjena polja na tablicama
desc vubstudenti;
select * from vubstudenti;

ALTER TABLE vubstudenti MODIFY(
   prezime  varchar2(40)
);

DROP INDEX IX_PREZIMENA_F;

ALTER TABLE vubstudenti MODIFY(
   prezime  varchar2(40)
);


desc vubstudenti
ALTER TABLE vubstudenti MODIFY(
   prezime  varchar2(15) NULL
);
desc vubstudenti

--primjer 10 komentari na tablice i polja
comment on table vubstudenti is 'Popis vubstudenta, tablica za testiranje održavanja tablica';
comment on column vubstudenti.jmbag is 'Jedinstveni matični broj akademskog građanina';
select * from all_tab_comments
where table_name = 'VUBSTUDENTI'






