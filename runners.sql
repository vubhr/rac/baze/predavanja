drop table runners;

drop sequence seq_runners;

CREATE SEQUENCE seq_runners START WITH 1 INCREMENT BY 1;

create table runners(
   ID number(9) PRIMARY KEY,
   username varchar(255) not null,
   password varchar(255) not null,
   yearOfBirth number(4) not null,
   gender number(1),
   created timestamp(6) default systimestamp,
   updated timestamp(6));
   
CREATE OR REPLACE TRIGGER trg_runners
BEFORE INSERT OR UPDATE ON runners
FOR EACH ROW
BEGIN
    IF :NEW.ID IS NULL THEN
        :NEW.ID := seq_runners.NEXTVAL;
    END IF;
    
    IF UPDATING THEN
        :NEW.updated := SYSTIMESTAMP;
    END IF;
END;
/


insert into RUNNERS (username, password, yearOfBirth, gender) values ('dpalfrey0', 'iS4<s>aWaRiP', 2008, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('akilliam1', 'uJ5~vBkV.0', 1909, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('acutten2', 'kR9"gpq)', 1924, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('lscully3', 'aF6!NF}P\H''(', 1969, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('jjeandon4', 'kE1~Kpsff', 2019, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('acotte5', 'mR0"~Smm(i\\1<', 1911, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('dsones6', 'oN2\xA3k8>I''Ku.j', 1997, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('iouthwaite7', 'kT2|~m/bK,<fAxoO', 2009, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('dportinari8', 'uZ5*98)bc,Wyf3g', 1971, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('dwhitlow9', 'xH6`FA>tEY>pw', 1992, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('cstringera', 'dP0$Rf>D1A', 1942, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('dcowerdb', 'pK3+i~p8w', 1922, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('mcodemanc', 'yJ6%._San', 1953, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('mboutcherd', 'kU1(/_X}\UqX~T=A', 1946, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('rguidie', 'kC9+yf@,"7WD', 1917, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('tdayesf', 'zE8"i<$F6K', 1900, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('iselvesterg', 'cO7=Xr>LJ9{wy!x', 1995, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('gcarvillh', 'hS7*f7U=', 1915, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('cmaccorkelli', 'qJ7`,B1*\kuTOwu', 1977, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('isametj', 'sK9"sxs3aGe.D3F', 1995, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('mboissierk', 'rN4''_??0', 1991, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('ctrippickl', 'nY1}B"$}D|q*%p$1', 1917, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('jtheuffm', 'bO0~m"AN<`T)W*K#', 1974, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('gsecrettn', 'yX5}JLniN6%', 1903, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('tlodemanno', 'sF3+dMYWbV3', 1930, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('bgossp', 'zN6!9Bqln|+PHZj', 1992, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('cpadwickq', 'dI0#pvQ8W|5R?v', 1946, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('lshealr', 'iP6A".{3?', 1994, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('fklostermans', 'uX8''k4EQBqVAz', 1954, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('slunckt', 'vA3~y{`''g!y{>Wd6', 1964, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('mpileticu', 'xD2#''m{+3M~', 1991, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('npaveyv', 'mE6,"M2b},', 1953, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('hlumw', 'mR9\lsWB<|''#8v$', 1990, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('bdallynx', 'oG8|H3zN$+q6', 1978, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('acunioy', 'lQ5*|*=Z3cDs@', 1940, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('cmeakz', 'rY2*`PJyh|LL', 1985, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('ndimmock10', 'oU8,$|0PEC<''$~j', 1951, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('cdilleway11', 'cC4*11Rn$L', 1937, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('rdibatista12', 'sE3#\UNE', 1956, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('mpiddlesden13', 'uA6"W/!7g)ctQm', 2017, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('cthouless14', 'dD1|A%f9@}{!l4', 1975, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('gteape15', 'qP9*B?te+BH', 1900, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('btejada16', 'dC5%TQK7h>', 1958, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('egason17', 'vE1`@yp''4G0J#Wrq', 1958, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('lhealey18', 'gI2*!JjkoGxo#NX', 1974, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('abigg19', 'pF5/f9SrlUTCyU~', 1953, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('bstraffon1a', 'mT8(v,wzp', 1951, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('bbucklee1b', 'dT8$B"Z(Vcd', 1965, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('himore1c', 'vZ4''5I.i', 1945, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('gvittore1d', 'hT5/QANM', 1972, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('xtroctor1e', 'rM4''evxc?g@."GO', 1914, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('astratz1f', 'cQ0_("k}j', 2018, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('rringer1g', 'hQ1,h0QSzf', 2013, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('stotaro1h', 'jB8.c"Pb(', 1972, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('bswancock1i', 'lQ7''euP/u%vw', 1919, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('glyal1j', 'aP5+FWdkJ1j7a', 2006, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('epenhallurick1k', 'cF5=gJej', 1987, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('rluscott1l', 'kW2A2eRw8KV', 1964, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('ggerardot1m', 'pP7>P''KREf', 1970, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('gchattelaine1n', 'wY0,Pv#r#', 2006, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('etrenbey1o', 'eT9~0l7SSF+C$.', 1926, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('cpykett1p', 'mA5*y"(Xht=,2vp', 1936, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('mberthome1q', 'kA3#P0HeDOc''"O', 2004, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('ptapping1r', 'zE2<<uO}5M', 1950, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('jgwatkin1s', 'xP9A<<{LG7', 1998, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('azecchinelli1t', 'rU3\wH9`r\oqH0I*', 1923, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('sjezard1u', 'vZ4{*<H{dHyJYA', 2019, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('twoolis1v', 'uD0*@''/''eP$@!V', 1902, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('rdochon1w', 'cO0*bOfFLgO', 1941, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('mbrandham1x', 'zF9AoaAm2"xRGLEk', 2002, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('mbruckshaw1y', 'fF0*$9(Qy7', 2007, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('pwoollett1z', 'mX3A{>3b`OWg=9', 1919, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('mmarie20', 'qZ0`>MXL(i.`{', 2014, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('clivesey21', 'qP7?ko2TsvO', 1909, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('pradenhurst22', 'qB3=E"N_#942o', 1992, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('colunney23', 'cY7\km''=u=F$K', 1968, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('shubbuck24', 'wT8@B>(ccUY*!EA#', 1916, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('brittmeyer25', 'wL4\ZSEl**~', 1980, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('rfussie26', 'nR6*9.n5A?b+ix5', 1983, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('jwelman27', 'lO3,awbVp8jD', 2014, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('audy28', 'dQ8#s(Arx">Tl', 1998, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('dkaliszewski29', 'nI3Ab@_S7', 1964, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('cmowle2a', 'zG3=\HaO', 1979, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('cnotley2b', 'bU7,P*C}oK,w''', 1998, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('rcaroll2c', 'tM7=ej+st', 1994, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('svalek2d', 'cE9AlQ2"yWT$\>', 2009, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('cglyne2e', 'gG7)KQ!F"1"''RZm', 1942, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('sdemeter2f', 'uF4+r!2x', 1943, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('nhansberry2g', 'oA8?7<.2g2', 2003, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('nstonelake2h', 'sJ4%Sxi%d"bboNd', 1984, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('taxelbey2i', 'kO9!(F>PM_y}w/', 1973, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('tchidlow2j', 'tB4{|l<6wJq9(', 1928, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('dpozer2k', 'lW9<)e`8a?Eo*U(I', 1937, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('vhaitlie2l', 'pI9$~W`g2KG', 1936, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('akimble2m', 'tS6!SMJb,@mRs', 2001, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('ykleynermans2n', 'qT8$OA(Pj2q%2)', 2001, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('dgodlip2o', 'eE6''ZdFts~EzXI=', 1909, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('wgarci2p', 'mQ0{zzC!3NH(l', 1944, 0);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('hcuttle2q', 'hJ9_uT7(U|<3l', 1974, 1);
insert into RUNNERS (username, password, yearOfBirth, gender) values ('tcoats2r', 'rD7*M@+9#~$i', 1944, 1);

select * from runners


SELECT r.*
FROM runners r
WHERE 
  CASE 
    WHEN EXTRACT(YEAR FROM SYSDATE) - r.yearOfBirth BETWEEN 15 AND 20 THEN 0
    WHEN EXTRACT(YEAR FROM SYSDATE) - r.yearOfBirth BETWEEN 21 AND 25 THEN 1
    WHEN EXTRACT(YEAR FROM SYSDATE) - r.yearOfBirth BETWEEN 26 AND 35 THEN 2
    WHEN EXTRACT(YEAR FROM SYSDATE) - r.yearOfBirth BETWEEN 36 AND 45 THEN 3
    WHEN EXTRACT(YEAR FROM SYSDATE) - r.yearOfBirth >= 55           THEN 4
    ELSE null
  END = 4
  
  
  
  select * from runners
  


  