create or replace NONEDITIONABLE PACKAGE DOHVATI AS 
 e_iznimka exception;
 PRAGMA EXCEPTION_INIT(e_iznimka, -20001); 

  procedure p_test(l_obj in out JSON_OBJECT_T);
  procedure p_runners(l_obj in out JSON_OBJECT_T);
  procedure p_runnersC(l_obj in out JSON_OBJECT_T);
  procedure p_login(l_obj in out JSON_OBJECT_T);
  procedure p_zupanije(l_obj in out JSON_OBJECT_T);

END DOHVATI;