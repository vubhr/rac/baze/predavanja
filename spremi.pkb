create or replace NONEDITIONABLE PACKAGE BODY SPREMI AS
e_iznimka exception;
--save_klijenti
-----------------------------------------------------------------------------------------
  procedure p_save_klijenti(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
      l_obj JSON_OBJECT_T;
      l_klijenti klijenti%rowtype;
      l_count number;
      l_id number;
      l_string varchar2(1000);
      l_search varchar2(100);
      l_page number; 
      l_perpage number;
      l_action varchar2(10);
  begin
  
     l_obj := JSON_OBJECT_T(in_json);  
     l_string := in_json.TO_STRING;
  
     SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.IME'),
        JSON_VALUE(l_string, '$.PREZIME' ),
        JSON_VALUE(l_string, '$.EMAIL' ),
        JSON_VALUE(l_string, '$.OIB' ),
        JSON_VALUE(l_string, '$.OVLASTI' ),
        JSON_VALUE(l_string, '$.SPOL' ),
        JSON_VALUE(l_string, '$.ZAPORKA' ),
        JSON_VALUE(l_string, '$.ACTION' )
    INTO
        l_klijenti.id,
        l_klijenti.IME,
        l_klijenti.PREZIME,
        l_klijenti.EMAIL,
        l_klijenti.OIB,
        l_klijenti.OVLASTI,
        l_klijenti.SPOL,
        l_klijenti.PASSWORD,
        l_action
    FROM 
       dual; 
       
    --FE kontrole
    if (nvl(l_action, ' ') = ' ') then
        /*if (filter.f_check_klijenti(l_obj, out_json)) then
           raise e_iznimka; 
        end if;*/ 
        null; 
    end if;
           
    if (l_klijenti.id is null) then
        begin
           insert into klijenti (IME, PREZIME, EMAIL, OIB, OVLASTI, SPOL, PASSWORD) values
             (l_klijenti.IME, l_klijenti.PREZIME,
              l_klijenti.EMAIL, l_klijenti.OIB, l_klijenti.OVLASTI,
              l_klijenti.SPOL, l_klijenti.PASSWORD);
           commit;
           
           l_obj.put('h_message', 'Uspješno ste unijeli klijenta'); 
           l_obj.put('h_errcode', 0);
           out_json := l_obj;
           
        exception
           when others then 
               COMMON.p_errlog('p_users',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
               rollback;
               raise;
        end;
    else
       if (nvl(l_action, ' ') = 'delete') then
           begin
               delete klijenti where id = l_klijenti.id;
               commit;    
               
               l_obj.put('h_message', 'Uspješno ste obrisali klijenta'); 
               l_obj.put('h_errcode', 0);
               out_json := l_obj;
            exception
               when others then 
                   COMMON.p_errlog('p_users',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
                   rollback;
                   raise;
            end;
       
       else
       
           begin
               update klijenti 
                  set IME = l_klijenti.IME,
                      PREZIME = l_klijenti.PREZIME,
                      EMAIL = l_klijenti.EMAIL,
                      OIB = l_klijenti.OIB,
                      OVLASTI = l_klijenti.OVLASTI, 
                      SPOL = l_klijenti.SPOL
               where
                  id = l_klijenti.id;
               commit;    
               
               l_obj.put('h_message', 'Uspješno ste promijenili klijenta'); 
               l_obj.put('h_errcode', 0);
               out_json := l_obj;
            exception
               when others then 
                   COMMON.p_errlog('p_users',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
                   rollback;
                   raise;
            end;
       end if;     
    end if;
    
    
  exception
     when e_iznimka then
        out_json := l_obj; 
     when others then
        COMMON.p_errlog('p_save_klijenti',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
        l_obj.put('h_message', 'Dogodila se greška u obradi podataka!'); 
        l_obj.put('h_errcode', 101);
        out_json := l_obj;
  END p_save_klijenti;

END SPREMI;