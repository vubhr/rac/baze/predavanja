--1 dohvatiti sva mjesta iz tablice naselja, a umjesto polja IDzupanija ispisivati naziv županije povezujući se s tablicom zupanije
select 
   n.mjesto, 
   n.postanskibroj, 
   n.opcina, 
  (select 
      z.zupanija 
   from 
      common.zupanije z 
   where 
      z.id = n.idzupanija) as zupanija
from 
   common.naselja n;

--2 Dohvatite sva naselja u Bjelovarsko-bilogorskoj županiji tako što ćete podupit iz županija postaviti u where uvjetu.
select 
   ID,
   mjesto,
   postanskibroj,
   opcina
from 
   common.naselja
where 
   idzupanija = (select 
                    id 
                 from 
                    common.zupanije 
                 where 
                    zupanija = 'BJELOVARSKO-BILOGORSKA');


--3 Dohvatite sva naselja iz Bjelovarsko-bilogorske županije i Virovitičko-podravske. Podupit iz županija postaviti u where uvjetu.
select 
   ID,
   mjesto,
   postanskibroj,
   opcina
from 
   common.naselja
where 
   idzupanija in (select 
                    id 
                 from 
                    common.zupanije 
                 where 
                    zupanija in ('BJELOVARSKO-BILOGORSKA', 'VIROVITIČKO-PODRAVSKA'));


--4 Dohvatite ime i prezime iz tablice common.korisnici i mjesto gdje žive.
select 
   concat(kor.ime, ' ' || kor.prezime) as naziv, 
  (select 
     n.mjesto
   from 
     common.naselja n
   where
     n.id = (select 
                kon.idnaselja
             from 
                common.kontakti kon
             where
                kon.idkorisnika = kor.id)) as mjesto
from 
   common.korisnici kor;

--5 kartezijev produkt
desc nastavnici;
desc predmeti;
select * from nastavnici;
select * from predmeti;

select * from nastavnici, predmeti;

select * from predmeti, nastavnici;

--6 spajanje po stranom ključu
select 
  * 
from 
  predmeti p,
  nastavnici n
where
  n.id = p.idnastavnika;


--7 spajanje po stranom ključu, redoslijed
select 
  * 
from 
  nastavnici n,
  predmeti p
where
  p.idnastavnika = n.id;


--8 ostali uvjeti filtriranja 
select 
  n.ime as ime,
  n.prezime as prezime,
  p.naziv as predmet,
  p.semestar as semestar
from 
  nastavnici n,
  predmeti p
where
  p.idnastavnika = n.id and
  p.semestar = 1;
  
--9 dohvatiti sva mjesta iz tablice naselja, a umjesto polja IDzupanija ispisivati naziv županije povezujući se s tablicom zupanije
select 
 n.mjesto,
 n.postanskibroj as pošta,
 n.opcina as općina,
 z.zupanija as županija
from 
  common.naselja n,
  common.zupanije z
where 
  z.id = n.idzupanija;


--10 Dohvatite sva naselja u Bjelovarsko-bilogorskoj županiji.
select 
 n.mjesto,
 n.postanskibroj as pošta,
 n.opcina as općina,
 z.zupanija as županija
from 
  common.naselja n,
  common.zupanije z
where 
  z.id = n.idzupanija and
  z.zupanija = 'BJELOVARSKO-BILOGORSKA';


--11 Dohvatite sva naselja iz Bjelovarsko-bilogorske županije i Virovitičko-podravske.
select 
 n.mjesto,
 n.postanskibroj as pošta,
 n.opcina as općina,
 z.zupanija as županija
from 
  common.naselja n,
  common.zupanije z
where 
  z.id = n.idzupanija and
  z.zupanija in ('BJELOVARSKO-BILOGORSKA', 'VIROVITIČKO-PODRAVSKA');


--12 Dohvatite ime i prezime iz tablice common.korisnici i mjesto gdje žive.
select
  concat(kor.ime, ' ' || kor.prezime) as naziv,
  n.mjesto as mjesto
from 
  common.korisnici kor,
  common.kontakti  kon,
  common.naselja   n
where
   kor.id = kon.idkorisnika and
   n.id = kon.idnaselja;