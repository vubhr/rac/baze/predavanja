drop table banke;
drop sequence banke_id_seq;
drop table doprinosi;
drop sequence DOPRINOSI_ID_SEQ;
drop table porezi;
drop sequence porezi_id_seq;
drop table konta;
drop sequence KONTA_ID_SEQ;
drop table temeljnice;
drop sequence TEMELJNICE_ID_SEQ;
drop table skladista;
drop sequence SKLADISTA_ID_SEQ;
drop table partneri;
drop SEQUENCE PARTNERI_ID_SEQ;
drop table poduzeca;
drop SEQUENCE PODUZECA_ID_SEQ;
drop table klijenti;
drop sequence klijenti_ID_SEQ;


CREATE TABLE klijenti (
	ID NUMBER(6, 0) NOT NULL,
	ime VARCHAR2(60) NOT NULL,
	prezime VARCHAR2(60) NOT NULL,
	OIB NUMBER(11, 0) UNIQUE NOT NULL,
	email VARCHAR2(60) UNIQUE NOT NULL,
	password VARCHAR2(20) NOT NULL,
	spol NUMBER(1, 0) NOT NULL,
	slika VARCHAR2(255) UNIQUE NULL,
	ovlasti NUMBER(1, 0) NOT NULL,
	constraint klijenti_PK PRIMARY KEY (ID));

CREATE sequence klijenti_ID_SEQ;

CREATE trigger BI_klijenti_ID
  before insert on klijenti
  for each row
begin
  select klijenti_ID_SEQ.nextval into :NEW.ID from dual;
end;
/
CREATE TABLE poduzeca (
	ID NUMBER(6, 0) NOT NULL,
  IDkorisnika NUMBER(6, 0) NOT NULL,
  IDnaselja number(6,0) NOT NULL,
	naziv VARCHAR2(60) NOT NULL,
	OIB NUMBER(11, 0) UNIQUE NOT NULL,
	adresa VARCHAR2(100) NOT NULL,
	constraint PODUZECA_PK PRIMARY KEY (ID));

CREATE sequence PODUZECA_ID_SEQ;

CREATE trigger BI_PODUZECA_ID
  before insert on poduzeca
  for each row
begin
  select PODUZECA_ID_SEQ.nextval into :NEW.ID from dual;
end;
/

CREATE TABLE partneri (
ID NUMBER(9, 0) NOT NULL,
IDpoduzeca NUMBER(9, 0) NOT NULL,
IDkorisnika NUMBER(9, 0) NOT NULL,
constraint PARTNERI_PK PRIMARY KEY (ID));

CREATE sequence PARTNERI_ID_SEQ;

CREATE trigger BI_PARTNERI_ID
  before insert on partneri
  for each row
begin
  select PARTNERI_ID_SEQ.nextval into :NEW.ID from dual;
end;
/

CREATE TABLE skladista (
   ID NUMBER(9,0) NOT NULL,
   IDpoduzeca NUMBER(9,0) NOT NULL,
   SIFRA VARCHAR2(20) UNIQUE NOT NULL, 
   naziv VARCHAR2(60) NOT NULL,
   MPVP NUMBER(1,0) NOT NULL,
   constraint SKLADISTA_PK PRIMARY KEY (ID));

CREATE sequence SKLADISTA_ID_SEQ;

CREATE trigger BI_SKLADISTA_ID
  before insert on skladista
  for each row
begin
  select SKLADISTA_ID_SEQ.nextval into :NEW.ID from dual;
end;
/

CREATE TABLE temeljnice (
ID NUMBER(9,0) NOT NULL,
IDKORISNIKA NUMBER(9,0) NOT NULL,
naziv VARCHAR2(60) NOT NULL,
SIFRA VARCHAR2(20) NOT NULL, 
constraint TEMELJNICE_PK PRIMARY KEY (ID));

CREATE sequence TEMELJNICE_ID_SEQ;

CREATE trigger BI_TEMELJNICE_ID
  before insert on temeljnice
  for each row
begin
  select TEMELJNICE_ID_SEQ.nextval into :NEW.ID from dual;
end;

/
CREATE TABLE konta (
   ID NUMBER(9,0) NOT NULL,
   IDkorisnika NUMBER(9,0) NOT NULL,
   SIFRA VARCHAR2(20) NOT NULL, 
   NAZIV VARCHAR2(60) NOT NULL,
constraint KONTA_PK PRIMARY KEY (ID));

CREATE sequence KONTA_ID_SEQ;

CREATE trigger BI_KONTA_ID
  before insert on konta
  for each row
begin
  select KONTA_ID_SEQ.nextval into :NEW.ID from dual;
end;

/

CREATE TABLE porezi (
ID NUMBER(9,0) NOT NULL,
naziv VARCHAR2(60) NOT NULL,
postotak NUMBER(5,2) NOT NULL,
constraint POREZI_PK PRIMARY KEY (ID));

CREATE sequence POREZI_ID_SEQ;

CREATE trigger BI_POREZI_ID
  before insert on porezi
  for each row
begin
  select POREZI_ID_SEQ.nextval into :NEW.ID from dual;
end;

/

CREATE TABLE doprinosi (
ID NUMBER(9, 0) NOT NULL,
NAZIV VARCHAR2(255) NOT NULL,
STOPA NUMBER(5,2) NOT NULL,
VRSTA VARCHAR2(255) NOT NULL,
constraint DOPRINOSI_PK PRIMARY KEY (ID));

CREATE sequence DOPRINOSI_ID_SEQ;

CREATE trigger BI_DOPRINOSI_ID
  before insert on doprinosi
  for each row
begin
  select DOPRINOSI_ID_SEQ.nextval into :NEW.ID from dual;
end;

/

CREATE TABLE banke (
ID NUMBER(9,0) NOT NULL,
IDnaselja NUMBER(9,0) NOT NULL,
naziv VARCHAR2(60) UNIQUE NOT NULL,
adresa VARCHAR2(100) UNIQUE NOT NULL,
constraint BANKE_PK PRIMARY KEY (ID));

CREATE sequence BANKE_ID_SEQ;

CREATE trigger BI_BANKE_ID
  before insert on banke
  for each row
begin
  select BANKE_ID_SEQ.nextval into :NEW.ID from dual;
end;

/


--ALTER TABLE NASELJA ADD CONSTRAINT  NASELJA_PK PRIMARY KEY (ID);
--ALTER TABLE ZUPANIJE ADD CONSTRAINT  ZUPANIJE_PK PRIMARY KEY (ID);
--ALTER TABLE NASELJA ADD CONSTRAINT naselja_fk0 FOREIGN KEY (IDzupanija) REFERENCES zupanije(ID);
--ALTER TABLE PRIREZI ADD CONSTRAINT  PRIREZI_PK PRIMARY KEY (ID);
ALTER TABLE konta ADD CONSTRAINT konta_fk0 FOREIGN KEY (IDkorisnika) REFERENCES klijenti(ID);
ALTER TABLE konta ADD CONSTRAINT konta_uk0 UNIQUE (IDKORISNIKA, SIFRA);
ALTER TABLE temeljnice ADD CONSTRAINT temeljnice_fk0 FOREIGN KEY (IDkorisnika) REFERENCES klijenti(ID);
ALTER TABLE temeljnice ADD CONSTRAINT temeljnice_uk0 UNIQUE (IDKORISNIKA, SIFRA);
ALTER TABLE poduzeca ADD CONSTRAINT poduzeca_fk0 FOREIGN KEY (IDkorisnika) REFERENCES klijenti(ID);
ALTER TABLE poduzeca ADD CONSTRAINT poduzeca_fk1 FOREIGN KEY (IDnaselja) REFERENCES naselja(ID);
ALTER TABLE banke ADD CONSTRAINT banke_fk1 FOREIGN KEY (IDnaselja) REFERENCES naselja(ID);
ALTER TABLE partneri ADD CONSTRAINT partneri_fk0 FOREIGN KEY (IDkorisnika) REFERENCES klijenti(ID);
ALTER TABLE partneri ADD CONSTRAINT partneri_fk1 FOREIGN KEY (IDpoduzeca) REFERENCES klijenti(ID);
ALTER TABLE partneri ADD CONSTRAINT skladista_fk0 FOREIGN KEY (IDpoduzeca) REFERENCES poduzeca(ID);

/

insert into klijenti (ime, prezime, OIB, email, password, spol, slika, ovlasti) values
                      ('Pavao', 'Šimić', 12345678911, 'psimic@vub.hr', '123', 0, 'slika1', 1);
insert into klijenti (ime, prezime, OIB, email, password, spol, slika, ovlasti) values
                      ('Zvonko', 'Jakovljević', 11987654321, 'mpreskocil@vub.hr', '123', 0, 'slika2', 1);                      
insert into klijenti (ime, prezime, OIB, email, password, spol, slika, ovlasti) values
                      ('Elena', 'Pavlić', 45875632147, 'pvukovic@vub.hr', '123', 0, 'slika3', 1);                                         
insert into klijenti (ime, prezime, OIB, email, password, spol, slika, ovlasti) values
                      ('Korana', 'Radić', 25687412369, 'msomljacan@vub.hr', '123', 0, 'slika4', 1);                                            

insert into poduzeca (IDkorisnika, IDnaselja, naziv, OIB, adresa) values
                      (1, 297,'Firma1', 12345678922, 'ulica 1');
insert into poduzeca (IDkorisnika, IDnaselja, naziv, OIB, adresa) values
                      (2, 297,'Firma2', 22987654321, 'ulica 2');                      
insert into poduzeca (IDkorisnika, IDnaselja, naziv, OIB, adresa) values
                      (3, 297,'Firma3', 58745632547, 'ulica 3');                      
insert into poduzeca (IDkorisnika, IDnaselja, naziv, OIB, adresa) values
                      (4, 297,'Firma4', 11258746987, 'ulica 4'); 


insert into partneri (IDkorisnika, IDpoduzeca) values
                      (1,2);                     
insert into partneri (IDkorisnika, IDpoduzeca) values
                      (1,4);                      
insert into partneri (IDkorisnika, IDpoduzeca) values
                      (2,3);                          
insert into partneri (IDkorisnika, IDpoduzeca) values
                      (2,1);    
insert into skladista (IDpoduzeca, sifra, NAZIV, MPVP) values
                      (1,'SK043F11', 'Skladište 11', 1);                          
insert into skladista (IDpoduzeca, sifra, NAZIV, MPVP) values
                      (1,'SK043F12', 'Skladište 12', 0);       
insert into skladista (IDpoduzeca, sifra, NAZIV, MPVP) values
                      (2,'SK043F21', 'Skladište 21', 1);                             
insert into skladista (IDpoduzeca, sifra, NAZIV, MPVP) values
                      (2,'SK043F22', 'Skladište 22', 0);                            
insert into skladista (IDpoduzeca, sifra, NAZIV, MPVP) values
                      (3,'SK043F31', 'Skladište 31', 1);     
insert into skladista (IDpoduzeca, sifra, NAZIV, MPVP) values
                      (3,'SK043F32', 'Skladište 32', 0);                           
insert into skladista (IDpoduzeca, sifra, NAZIV, MPVP) values
                      (4,'SK043F41', 'Skladište 41', 1);     
insert into skladista (IDpoduzeca, sifra, NAZIV, MPVP) values
                      (4,'SK043F42', 'Skladište 42', 0);                           
                      
insert into temeljnice (IDKORISNIKA, NAZIV, SIFRA) values
                      (1,'Obveze prema banci', 'TEOBVB11');                           
insert into temeljnice (IDKORISNIKA, NAZIV, SIFRA) values
                      (1,'Obveze prema državi', 'TEOBVD12');                                                 
insert into temeljnice (IDKORISNIKA, NAZIV, SIFRA) values
                      (2,'Obveze prema banci', 'TEOBVB21');                           
insert into temeljnice (IDKORISNIKA, NAZIV, SIFRA) values
                      (2,'Obveze prema državi', 'TEOBVD22');                                                                       
insert into temeljnice (IDKORISNIKA, NAZIV, SIFRA) values
                      (3,'Obveze prema banci', 'TEOBVB31');                           
insert into temeljnice (IDKORISNIKA, NAZIV, SIFRA) values
                      (3,'Obveze prema državi', 'TEOBVD32');                                                                       
insert into temeljnice (IDKORISNIKA, NAZIV, SIFRA) values
                      (4,'Obveze prema banci', 'TEOBVB41');                           
insert into temeljnice (IDKORISNIKA, NAZIV, SIFRA) values
                      (4,'Obveze prema državi', 'TEOBVD42'); 

insert into konta (IDKORISNIKA, SIFRA, NAZIV) values
                      (1,'KT12311', 'Glavna knjiga');                         
insert into konta (IDKORISNIKA, SIFRA, NAZIV) values
                      (1,'KT12312', 'Žiro račun');                                               
insert into konta (IDKORISNIKA, SIFRA, NAZIV) values
                      (2,'KT12321', 'Glavna knjiga');                         
insert into konta (IDKORISNIKA, SIFRA, NAZIV) values
                      (2,'KT12322', 'Žiro račun');                        
insert into konta (IDKORISNIKA, SIFRA, NAZIV) values
                      (3,'KT12331', 'Glavna knjiga');                         
insert into konta (IDKORISNIKA, SIFRA, NAZIV) values
                      (3,'KT12332', 'Žiro račun');                        
insert into konta (IDKORISNIKA, SIFRA, NAZIV) values
                      (4,'KT12341', 'Glavna knjiga');                         
insert into konta (IDKORISNIKA, SIFRA, NAZIV) values
                      (4,'KT12342', 'Žiro račun');
insert into porezi (naziv, postotak) values
                   ('porez na dobit', 12); 
insert into porezi (naziv, postotak) values
                   ('PDV', 25); 
insert into doprinosi(NAZIV, STOPA, VRSTA) values
                   ('Nabava', 10, 'Za radnika'); 
insert into doprinosi(NAZIV, STOPA, VRSTA) values
                   ('Prema državi', 12, 'Stopa doprinosa');
insert into banke(IDnaselja, NAZIV, ADRESA) values
                   (297, 'Zagrebačka banka', 'Adresa 12'); 
insert into banke(IDnaselja, NAZIV, ADRESA) values
                   (297, 'Erste banka', 'Adresa 14');                    

commit;

--select * from klijenti