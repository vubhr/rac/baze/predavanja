--ako postoji dropaj, ako ne postoji javlja greĹˇku, table or view does not exist
drop table studenti;
DROP SEQUENCE seq_korisnici_id;
--1. primjer default i not null
create table studenti(
   jmbag    number(8)     NOT NULL,
   prezime  varchar2(20)  NOT NULL,
   ime      varchar2(20)  NOT NULL,
   redovni  number(1)     DEFAULT 1 NOT NULL);
insert into studenti (jmbag, prezime, ime) values (12345, 'Mićković', 'Vlatko');
select * from studenti;
--2. primjer default, null i not null
drop table studenti;
create table studenti(
   jmbag    number(8)     NOT NULL,
   prezime  varchar2(20)  NOT NULL,
   ime      varchar2(20)  NOT NULL,
   redovni  number(1)     DEFAULT 1 NOT NULL,
   mobitel  number(12)    NULL,
   email    varchar2(40));
   
select * from studenti;
insert into studenti (jmbag, prezime, ime) values (12345, 'Vukoja', 'Oliver');
    
--3. primjer, nismo specificirali NOT NULL za polje REDOVNI
drop table studenti;
create table studenti(
   jmbag    number(8)     NOT NULL,
   prezime  varchar2(20)  NOT NULL,
   ime      varchar2(20)  NOT NULL,
   redovni  number(1)     DEFAULT 1,
   mobitel  number(12)    NULL,
   email    varchar2(40));
   
select * from studenti;
insert into studenti (jmbag, prezime, ime) values (12345, 'Vukoja', 'Oliver');
insert into studenti (jmbag, prezime, ime, redovni) values (12345, 'Zorić', 'Tomislav', null);

--4. primjer 
insert into studenti (jmbag, prezime) values (12345, 'Skender');
insert into studenti (jmbag, prezime, ime) values (12345, 'Skender', '');
--5. primjer
select * from studenti;
update studenti set email = 'nepoznato';
update studenti set email = null;
--6. primjer
drop table prodaja
select * from prodaja
create table prodaja(
  id                  number(10)  NOT NULL,
  IDkorisnika         number(10),
  datum               DATE     default sysdate not null,
  cijena              number(10,2),
  porez               number(10,2),
  dostava             number(10,2),
  ukupno              number(10,2));

insert into prodaja
   (id, IDkorisnika, cijena, porez, dostava, ukupno)
values
   (78432, 73298, 500, 125, 5, 630);
--7. primjer
drop table prodaja;
select * from prodaja;
create table prodaja(
  id              number(10)  NOT NULL,
  IDkorisnika     number(10),
  datum           DATE     default sysdate not null,
  cijena          number(10,2),
  porez           number(10,2),
  dostava         number(10,2),
  ukupno          number(10,2) 
                          as (cijena + porez + dostava) VIRTUAL);


insert into prodaja
   (id, IDkorisnika, cijena, porez, dostava)
values
   (78432, 73298, 500, 125, 5);
update prodaja set dostava = 2 where id = 78432;

--8. primjer
drop table korisnici;
select * from korisnici
create table korisnici(
  id                   number(10)  NOT NULL,
  ime                  varchar2(20),
  prezime              varchar2(20),
  email                varchar2(60),
  domena               varchar2(60 CHAR) as (substr(email,   
  instr(email, '@', 1,1)+1)) virtual);
insert into korisnici 
   (id, ime, prezime, email)
values
   (73298, 'Tomislav', 'Adamović', 'tadamovic@vub.hr');

--9. primjer
SELECT substr('tadamovic@vub.hr', instr('tadamovic@vub.hr', '@', 1,1)+1) from dual;
--10. primjer
drop table studenti;
select * from studenti;
create table studenti(
   jmbag     number(8)     NOT NULL,
   prezime   varchar2(20)  NOT NULL,
   ime       varchar2(20)  NOT NULL,
   redovni   number(1)     DEFAULT 1 NOT NULL,
   mobitel   number(12)    NULL,
   email    varchar2(40)   NULL,
   CONSTRAINT pk_studenti PRIMARY KEY (jmbag));
insert into studenti (jmbag, prezime, ime) values (12345, 'Vukoja', 'Oliver');
insert into studenti (jmbag, prezime, ime) values (12345, 'Zorić', 'Tomislav');
insert into studenti (jmbag, prezime, ime) values (12346, 'Zorić', 'Tomislav');
--11. primjer
ALTER TABLE studenti
ADD CONSTRAINT pk_ime primary key (ime)

--12. primjer
ALTER TABLE studenti
ADD CONSTRAINT pk_ime unique (ime);
insert into studenti (jmbag, prezime, ime) values (12347, 'Skender', 'Oliver');
--13. primjer
CREATE SEQUENCE seq_studenti_id
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  START WITH 12347
  INCREMENT BY 1
  CACHE 20; 

insert into studenti (jmbag, prezime, ime) values (SEQ_STUDENTI_ID.NEXTVAL, 'Skender', 'Miroslav'); 
select seq_studenti_id.nextval from dual;


CREATE SEQUENCE seq_korisnici_id
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  START WITH 73298
  INCREMENT BY 1
  CACHE 20; 











  











