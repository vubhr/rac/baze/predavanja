--1
SET SERVEROUTPUT ON
DECLARE
   l_char char := 'AB';
BEGIN
   dbms_output.put_line('Tekst je ' || l_char);
END;

--2
DECLARE
  l_char varchar2 := 'A';
BEGIN
   dbms_output.put_line('Tekst je ' || l_char);
END;

--3
select  current_timestamp, systimestamp from dual;

--4
DECLARE
   l_today_date        DATE := SYSDATE;
   l_today_timestamp   TIMESTAMP(6) := SYSTIMESTAMP;
   l_today_timetzone   TIMESTAMP WITH TIME ZONE := SYSTIMESTAMP;
BEGIN
   dbms_output.put_line('l_today_date ' || l_today_date);
   dbms_output.put_line('l_today_timestamp ' || l_today_timestamp);
   dbms_output.put_line('l_today_timetzone ' || l_today_timetzone);   
END;

--5
DECLARE
   l_date date;      
BEGIN
   l_date := to_date ('07.01.2013 12:00:00', 'MM.DD.YYYY HH24:MI:SS');
   dbms_output.put_line('Datum je ' || l_date);
END;

--6
select to_char(sysdate, 'MONTH mon MM') as month  from dual;

--7
select systimestamp(9) from dual;

--8
set serveroutput on
declare
  l_tmpstp timestamp(6);
begin
   l_tmpstp := to_timestamp('01.07.2020. 12:01:01.123456', 'DD.MM.YYYY. HH24:MI:SS:FF');
   dbms_output.put_line('Vrijeme je ' || l_tmpstp);
end;

--9
set serveroutput on
declare
  l_boolean BOOLEAN := TRUE;
begin
   if(l_boolean) then
      dbms_output.put_line('Točno');
   else
      dbms_output.put_line('Netočno');
   end if;
end;

--10
set serveroutput on
declare
  TYPE r_korisnici IS RECORD(
    ime varchar2(40),
    prezime varchar2(40),
    email varchar2(40),
    adresa varchar2(60),
    godiste number(4));
  l_marko r_korisnici;  
  l_new_line varchar(1) := chr(10);
begin
   l_marko.ime := 'Marko';
   l_marko.prezime := 'Marković';
   l_marko.email := 'mmarkovic@vub.hr';
   l_marko.adresa := 'Trg Eugena Kvaternika 4 43000 Bjelovar';
   l_marko.godiste := to_number(extract(year from sysdate)) - 18;
   
   dbms_output.put_line('Korisnik: ' || l_new_line ||
                        l_marko.ime || ' ' || l_marko.prezime || l_new_line ||
                        l_marko.adresa || l_new_line ||
                        'godiste: ' || l_marko.godiste);
end;

--11
set serveroutput on
declare
  l_neka_varijabla varchar(20) := 'Varijabla 1';
  l_varijabla l_neka_varijabla%type := 'Varijabla 2';
  l_boja colors.naziv%type := 'Yellow';
  l_new_line varchar(1) := chr(10);
begin
      
   dbms_output.put_line('1. ' || l_neka_varijabla || l_new_line ||
                        '2. ' || l_varijabla || l_new_line ||
                        l_boja || l_new_line);
end;

--12
set serveroutput on
declare
  l_colors colors%rowtype;
  l_new_line varchar(1) := chr(10);
begin
   l_colors.ID := 885;   
   l_colors.SIFRA := 'Zelena';   
   l_colors.NAZIV := 'Zelena';   
   l_colors.COLCOD := '#00FF00';   
   l_colors.RED := 0;   
   l_colors.GREEN := 255;   
   l_colors.BLUE := 0;   
   
   dbms_output.put_line('Boja: ' || l_new_line ||
                        l_colors.ID || l_new_line ||
                        l_colors.SIFRA || l_new_line ||
                        l_colors.NAZIV || l_new_line ||
                        l_colors.COLCOD || l_new_line ||
                        l_colors.RED || l_new_line ||
                        l_colors.GREEN || l_new_line ||
                        l_colors.BLUE);

end;

--13
set serveroutput on
declare
   l_id zupanije.id%type;
   l_zupanija zupanije.zupanija%type;
begin
   SELECT
      id,
      zupanija
   into
      l_id,
      l_zupanija
   from
      zupanije
   where
      id = 14;
   dbms_output.put_line(l_id || ' ' || l_zupanija);   
end;

--14
set serveroutput on
declare
   l_id zupanije.id%type;
   l_zupanija zupanije.zupanija%type;
begin
   SELECT
      id,
      zupanija
   into
      l_id,
      l_zupanija
   from
      zupanije;
   dbms_output.put_line(l_id || ' ' || l_zupanija);   
exception
   when no_data_found then
     dbms_output.put_line('Nema podataka');   
   when too_many_rows then
     dbms_output.put_line('Ne može više vrijednosti u jednu varijablu');   
   when others then
     dbms_output.put_line('Dogodila se neočekivana greška');    
end;

--15
set serveroutput on
declare
   l_zupanije zupanije%rowtype;
begin
   SELECT
      *
   into
      l_zupanije
   from
      zupanije
   where
      id = 14;
   dbms_output.put_line(l_zupanije.id || ' ' || l_zupanije.zupanija);   
exception
   when no_data_found then
     dbms_output.put_line('Nema podataka');   
   when too_many_rows then
     dbms_output.put_line('Ne može više vrijednosti u jednu varijablu');   
   when others then
     dbms_output.put_line('Dogodila se neočekivana greška');    
end;

--16

set serveroutput on
declare
   l_zupanije zupanije%rowtype;
begin
   SELECT
      *
   into
      l_zupanije
   from
      zupanije
   where
      id = 14;
   
   if l_zupanije.zupanija = 'BJELOVARSKO-BILOGORSKA' THEN
      dbms_output.put_line('Naša županija');   
   end if;
exception
   when no_data_found then
     dbms_output.put_line('Nema podataka');   
   when too_many_rows then
     dbms_output.put_line('Ne može više vrijednosti u jednu varijablu');   
   when others then
     dbms_output.put_line('Dogodila se neočekivana greška');    
end;

--17
set serveroutput on
declare
   l_zupanije zupanije%rowtype;
begin
   SELECT
      *
   into
      l_zupanije
   from
      zupanije
   where
      id = 11;
   if l_zupanije.id in (7,8,9,10,11) THEN
      dbms_output.put_line('Slavonske županije');   
   end if;
exception
   when no_data_found then
     dbms_output.put_line('Nema podataka');   
   when too_many_rows then
     dbms_output.put_line('Ne može više vrijednosti u jednu varijablu');   
   when others then
     dbms_output.put_line('Dogodila se neočekivana greška');    
end;

--18
set serveroutput on
declare
   l_zupanije zupanije%rowtype;
begin
   SELECT
      *
   into
      l_zupanije
   from
      zupanije
   where
      id = 8;
   if (l_zupanije.id = 7) or
      (l_zupanije.id = 8) or
      (l_zupanije.id = 9) or
      (l_zupanije.id = 10) or
      (l_zupanije.id = 11) then
      dbms_output.put_line('Slavonske županije');   
   end if;
exception
   when no_data_found then
     dbms_output.put_line('Nema podataka');   
   when too_many_rows then
     dbms_output.put_line('Ne može više vrijednosti u jednu varijablu');   
   when others then
     dbms_output.put_line('Dogodila se neočekivana greška');    
end;

--19
set serveroutput on
declare
   l_zupanije zupanije%rowtype;
begin
   SELECT
      *
   into
      l_zupanije
   from
      zupanije
   where
      id = 8;
   if (l_zupanije.id = 14) then
      dbms_output.put_line('Naša županija');
   else
      dbms_output.put_line('Nije naša županija');
   end if;
exception
   when no_data_found then
     dbms_output.put_line('Nema podataka');   
   when too_many_rows then
     dbms_output.put_line('Ne može više vrijednosti u jednu varijablu');   
   when others then
     dbms_output.put_line('Dogodila se neočekivana greška');    
end;

--20
set serveroutput on
declare
   l_zupanije zupanije%rowtype;
begin
   SELECT
      *
   into
      l_zupanije
   from
      zupanije
   where
      id = 8;
   if (l_zupanije.id = 1) then
      dbms_output.put_line('Grad Zagreb');
   elsif (l_zupanije.id = 2) then
      dbms_output.put_line('Zagrebačka županija');
   else
      dbms_output.put_line('Nije okolica grada Zagreba');
   end if;
   dbms_output.put_line(l_zupanije.id || ' ' || l_zupanije.zupanija);
   
exception
   when no_data_found then
     dbms_output.put_line('Nema podataka');   
   when too_many_rows then
     dbms_output.put_line('Ne može više vrijednosti u jednu varijablu');   
   when others then
     dbms_output.put_line('Dogodila se neočekivana greška');    
end;

--21
set serveroutput on
declare
   l_zupanije zupanije%rowtype;
begin
   SELECT
      *
   into
      l_zupanije
   from
      zupanije
   where
      id = 8;
   case l_zupanije.id
   when 1 then   
      dbms_output.put_line('Grad Zagreb');
   when 2 then
      dbms_output.put_line('Zagrebačka županija');
   else
      dbms_output.put_line('Nije okolica grada Zagreba');
   end case;
   dbms_output.put_line(l_zupanije.id || ' ' || l_zupanije.zupanija);
   
exception
   when no_data_found then
     dbms_output.put_line('Nema podataka');   
   when too_many_rows then
     dbms_output.put_line('Ne može više vrijednosti u jednu varijablu');   
   when others then
     dbms_output.put_line('Dogodila se neočekivana greška');    
end;

--22
set serveroutput on
declare
   l_zupanije zupanije%rowtype;
   l_rezultat varchar2(200);
begin
   SELECT
      *
   into
      l_zupanije
   from
      zupanije
   where
      id = 8;
   l_rezultat := 
   case l_zupanije.id
   when 1 then   
      'Grad Zagreb'
   when 2 then
      'Zagrebačka županija'
   else
      'Nije okolica grada Zagreba'
   end;
   dbms_output.put_line(l_zupanije.id || ' ' || l_zupanije.zupanija);
   
exception
   when no_data_found then
     dbms_output.put_line('Nema podataka');   
   when too_many_rows then
     dbms_output.put_line('Ne može više vrijednosti u jednu varijablu');   
   when others then
     dbms_output.put_line('Dogodila se neočekivana greška');    
end;


--23
set serveroutput on
declare
   l_counter number := 0;
   l_sum number := 0;
begin
   loop
      l_sum := l_sum + l_counter;
      l_counter := l_counter + 1;
      dbms_output.put_line('Brojač je:' || l_counter);
      exit when l_counter > 3;
   end loop;
exception
   when no_data_found then
     dbms_output.put_line('Nema podataka');   
   when too_many_rows then
     dbms_output.put_line('Ne može više vrijednosti u jednu varijablu');   
   when others then
     dbms_output.put_line('Dogodila se neočekivana greška');    
end;

--24
set serveroutput on
declare
   l_counter number := 0;
   l_sum number := 0;
begin
   loop
      l_sum := l_sum + l_counter;
      l_counter := l_counter + 1;
      dbms_output.put_line('Brojač je:' || l_counter);
      exit;
   end loop;
exception
   when no_data_found then
     dbms_output.put_line('Nema podataka');   
   when too_many_rows then
     dbms_output.put_line('Ne može više vrijednosti u jednu varijablu');   
   when others then
     dbms_output.put_line('Dogodila se neočekivana greška');    
end;  

--25
set serveroutput on
declare
   l_counter number := 0;
   l_sum number := 0;
begin
   loop
      l_sum := l_sum + l_counter;
      l_counter := l_counter + 1;
      dbms_output.put_line('Brojač je:' || l_counter);
      return;
   end loop;
exception
   when no_data_found then
     dbms_output.put_line('Nema podataka');   
   when too_many_rows then
     dbms_output.put_line('Ne može više vrijednosti u jednu varijablu');   
   when others then
     dbms_output.put_line('Dogodila se neočekivana greška');    
end; 

--26
set serveroutput on
declare
   l_counter number := 0;
   l_sum number := 0;
begin
   loop
      l_sum := l_sum + l_counter;
      l_counter := l_counter + 1;
      dbms_output.put_line('Brojač je:' || l_counter || ' ' || 'Suma je: ' || l_sum);
      if l_sum > 5 then
         return;
      end if;
   end loop;
exception
   when no_data_found then
     dbms_output.put_line('Nema podataka');   
   when too_many_rows then
     dbms_output.put_line('Ne može više vrijednosti u jednu varijablu');   
   when others then
     dbms_output.put_line('Dogodila se neočekivana greška');    
end; 

--27
set serveroutput on
declare
   l_sum number := 0;
begin
   for l_counter in 1..3
   loop
      l_sum := l_sum + l_counter;
      dbms_output.put_line('Brojač je:' || l_counter || ' ' || 'Suma je: ' || l_sum);
   end loop;
exception
   when no_data_found then
     dbms_output.put_line('Nema podataka');   
   when too_many_rows then
     dbms_output.put_line('Ne može više vrijednosti u jednu varijablu');   
   when others then
     dbms_output.put_line('Dogodila se neočekivana greška');    
end; 

--28
set serveroutput on
declare
   l_sum number := 0;
begin
   for l_counter in REVERSE 1..3
   loop
      l_sum := l_sum + l_counter;
      dbms_output.put_line('Brojač je:' || l_counter || ' ' || 'Suma je: ' || l_sum);
   end loop;
exception
   when no_data_found then
     dbms_output.put_line('Nema podataka');   
   when too_many_rows then
     dbms_output.put_line('Ne može više vrijednosti u jednu varijablu');   
   when others then
     dbms_output.put_line('Dogodila se neočekivana greška');    
end; 

--29
set serveroutput on
declare
   l_sum number := 0;
begin
   for l_counter in REVERSE 1..3
   loop
      l_sum := l_sum + l_counter;
      dbms_output.put_line('Brojač je:' || l_counter || ' ' || 'Suma je: ' || l_sum);
   end loop;
   dbms_output.put_line('Brojač je:' || l_counter || ' ' || 'Suma je: ' || l_sum);
exception
   when no_data_found then
     dbms_output.put_line('Nema podataka');   
   when too_many_rows then
     dbms_output.put_line('Ne može više vrijednosti u jednu varijablu');   
   when others then
     dbms_output.put_line('Dogodila se neočekivana greška');    
end; 

--30
set serveroutput on
declare
   l_sum number := 0;
   l_counter number := 500;
begin
   for l_counter in REVERSE 1..3
   loop
      l_sum := l_sum + l_counter;
      dbms_output.put_line('Brojač je:' || l_counter || ' ' || 'Suma je: ' || l_sum);
   end loop;
   dbms_output.put_line('Brojač je:' || l_counter || ' ' || 'Suma je: ' || l_sum);
exception
   when no_data_found then
     dbms_output.put_line('Nema podataka');   
   when too_many_rows then
     dbms_output.put_line('Ne može više vrijednosti u jednu varijablu');   
   when others then
     dbms_output.put_line('Dogodila se neočekivana greška');    
end; 

--31
set serveroutput on
declare
   l_sum number := 0;
   l_lower number := 2;
   l_upper number := 5;
begin
   for l_counter in l_lower..l_upper
   loop
      l_sum := l_sum + l_counter;
      dbms_output.put_line('Brojač je:' || l_counter || ' ' || 'Suma je: ' || l_sum);
   end loop;
exception
   when no_data_found then
     dbms_output.put_line('Nema podataka');   
   when too_many_rows then
     dbms_output.put_line('Ne može više vrijednosti u jednu varijablu');   
   when others then
     dbms_output.put_line('Dogodila se neočekivana greška');    
end; 

--32
set serveroutput on
declare
   l_sum number := 0;
   l_lower number := 1;
   l_upper number := 5;
   l_step_counter number;
begin
   for l_counter in l_lower..l_upper
   loop
      l_step_counter := l_counter*2;
      dbms_output.put_line('Brojač je:' || l_step_counter);
   end loop;
exception
   when no_data_found then
     dbms_output.put_line('Nema podataka');   
   when too_many_rows then
     dbms_output.put_line('Ne može više vrijednosti u jednu varijablu');   
   when others then
     dbms_output.put_line('Dogodila se neočekivana greška');    
end; 

--33
set serveroutput on
declare
   l_lower number := 1;
   l_upper number;
begin
   SELECT
      COUNT(1)
   INTO
      L_UPPER
   FROM 
      ZUPANIJE;
   
   for l_counter in l_lower..l_upper
   loop
      dbms_output.put_line('Brojač je:' || l_counter);
   end loop;
exception
   when no_data_found then
     dbms_output.put_line('Nema podataka');   
   when too_many_rows then
     dbms_output.put_line('Ne može više vrijednosti u jednu varijablu');   
   when others then
     dbms_output.put_line('Dogodila se neočekivana greška');    
end; 

--34
set serveroutput on
declare
   l_count number := 6;
begin
   
   for l_counter in 1..l_count
   loop
      if l_counter = 5 then
         continue;
      end if; 
      dbms_output.put_line('Brojač je:' || l_counter);
   end loop;
exception
   when no_data_found then
     dbms_output.put_line('Nema podataka');   
   when too_many_rows then
     dbms_output.put_line('Ne može više vrijednosti u jednu varijablu');   
   when others then
     dbms_output.put_line('Dogodila se neočekivana greška');    
end; 

--35
set serveroutput on
begin
   
   for l_first_counter in 1..2
   loop
      dbms_output.put_line('Vanjska petlja:' || l_first_counter);
      for l_second_counter in 1..3
      loop
         dbms_output.put_line('Unutrašnja petlja:' || l_second_counter);
      end loop;
   end loop;
exception
   when no_data_found then
     dbms_output.put_line('Nema podataka');   
   when too_many_rows then
     dbms_output.put_line('Ne može više vrijednosti u jednu varijablu');   
   when others then
     dbms_output.put_line('Dogodila se neočekivana greška');    
end; 




