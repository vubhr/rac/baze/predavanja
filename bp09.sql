drop table studenti1
drop table predmeti1
drop table nastavnicip
drop table upisi1
drop table opcine
--1 Nova tablica studenti1
select * from studenti
create table studenti1 as
(select * from studenti)
select * from studenti1

--2 pojedina polja
select * from predmeti
create table predmeti1 as
(select 
   ID, 
   IDNASTAVNIKA, 
   NAZIV, 
   SEMESTAR
from
   predmeti)
select * from predmeti1
--3 više tablica
create table nastavnicip as
(select 
   n.ID,
   n.IME,
   n.PREZIME,
   p.NAZIV,
   p.ECTS,
   p.SEMESTAR
 from 
   nastavnici n,
   predmeti p
 where 
   n.ID = p.IDNASTAVNIKA)
select * from nastavnicip
--4 bez podataka
select * from upisi
create table upisi1 as
(select
   *
 from 
   upisi 
 where 1=2)
select * from upisi1
--5 tablica opcine
create table opcine as
(select 
   *
 from 
   common.naselja 
 where 
   upper(rtrim(opcina)) = upper(rtrim(mjesto)))
--6 insert u tablicu opcine dodajmo i sva mjesto iz BBŽ
insert into opcine
 select 
    n.ID,
    n.MJESTO,
    n.POSTANSKIBROJ,
    n.OPCINA,
    n.IDZUPANIJA
 from 
    common.naselja n, 
    common.zupanije z
 where 
   z.id = n.idzupanija and
   z.zupanija = 'BJELOVARSKO-BILOGORSKA' and
   upper(rtrim(n.opcina)) != upper(rtrim(n.mjesto))
--7 insert mjesta iz Osječko-Baranjske županije
select 
  * 
from 
  opcine 
where 
  idzupanija = 7

 select 
   *
 from common.naselja n where not exists
 (select 
     *
  from 
     opcine o
  where 
    o.id = n.id)
  and n.idzupanija = 7  
  
insert into opcine  
select 
   *
 from common.naselja n where not exists
 (select 
     *
  from 
     opcine o
  where 
    o.id = n.id)
  and n.idzupanija = 7    

--8 promijenimo svim studentima zaporke
select * from studenti
update studenti set password = 'koliko99' 
--9 povećajmo broj ectsa za 1 na svim predmetima
select * from predmeti
update predmeti set ects = ects + 1 
--10 Neka Romana Knežić predaje i Uvod u programiranje
select 
  * 
from 
   nastavnici n, 
   predmeti p  
where
   n.id = p.idnastavnika
   
--11 
  update 
     predmeti 
  set 
    idnastavnika = 
       (select 
          id 
        from 
          nastavnici 
        where 
          IME = 'Romana' and 
          prezime = 'Knežić')
  where 
    naziv = 'Uvod u programiranje'
--12 commit i rollback
update 
  nastavnici 
set 
  ime = 'Andrija'
select * from nastavnici

rollback;
--13 svi koji su upisali Matematiku 2, a nisu je položili, upisat će im se ocjena 5
select * from upisi
select * from predmeti
update upisi u
set ocjena = 5 
where  
  exists
 (select 
     *
  from 
     predmeti p
  where 
     p.id = u.idpredmeta and 
     p.naziv = 'Matematika 2' and
     ocjena is null)

--14 
delete from upisi u
where  
  exists
 (select 
     *
  from 
     predmeti p
  where 
     p.id = u.idpredmeta and 
     p.naziv = 'Matematika 2' and
     ocjena is null)
select * from upisi
--15 brisanje podataka iz tablice upisi
delete from upisi
--16 polja u tablici colors
desc colors
--17
INSERT INTO colors (SIFRA, NAZIV, COLCOD, RED, GREEN, BLUE, UPDATED, CREATED) 
VALUES ('bijela', 'bijela', '#FFF', 255, 255, 255,SYSTIMESTAMP, SYSTIMESTAMP);
SELECT * FROM COLORS ORDER BY ID DESC
--18 
INSERT INTO colors (SIFRA, NAZIV, COLCOD, RED, GREEN, BLUE) 
VALUES ('malo manje bijela', 'malo manje bijela', 'fffff0', 255, 255, 240);

SELECT * FROM COLORS ORDER BY ID DESC

 
 
   



