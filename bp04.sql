--primjer1
--kreiranje tablice studenti
DROP TABLE STUDENTI;
DROP SEQUENCE STUDENTI_ID_SEQ;
CREATE TABLE studenti (
	ID NUMBER(9,0) NOT NULL,
	JMBAG NUMBER(8,0) NOT NULL,
	PREZIME VARCHAR2(20) NOT NULL,
	IME VARCHAR2(20) NOT NULL,
	REDOVNI NUMBER(1,0) NOT NULL,
	GODISTE NUMBER(4,0) NOT NULL,
	SPOL NUMBER(1,0) NOT NULL,
	POZMOB NUMBER(3,0) NOT NULL,
	MOBITEL NUMBER(7,0) NOT NULL,
	EMAIL VARCHAR2(40) NOT NULL,
	constraint STUDENTI_PK PRIMARY KEY (ID));

CREATE sequence STUDENTI_ID_SEQ;

CREATE trigger BI_STUDENTI_ID
  before insert on studenti
  for each row
begin
  select STUDENTI_ID_SEQ.nextval into :NEW.ID from dual;
end;

/
--primjer2
--unos podataka u tablicu
Insert into STUDENTI (JMBAG,PREZIME,IME,REDOVNI,GODISTE,SPOL,POZMOB,MOBITEL,EMAIL) values (12367,'Vukoja','Oliver',1,1997,0,98,587451,'OVUKOJA@VUB.HR');
Insert into STUDENTI (JMBAG,PREZIME,IME,REDOVNI,GODISTE,SPOL,POZMOB,MOBITEL,EMAIL) values (12368,'Zorić','Tomislav',1,1995,0,98,685973,'TZORIC@VUB.HR');
Insert into STUDENTI (JMBAG,PREZIME,IME,REDOVNI,GODISTE,SPOL,POZMOB,MOBITEL,EMAIL) values (12369,'Kopčić','Josip',1,1997,0,95,574123,'JKOPCIC@VUB.HR');
Insert into STUDENTI (JMBAG,PREZIME,IME,REDOVNI,GODISTE,SPOL,POZMOB,MOBITEL,EMAIL) values (12370,'Marinić','Jelena',1,1996,1,91,564873,'JMARINIC@VUB.HR');
Insert into STUDENTI (JMBAG,PREZIME,IME,REDOVNI,GODISTE,SPOL,POZMOB,MOBITEL,EMAIL) values (12371,'Arnold','Mirela',1,1994,1,91,147985,'MARNOLD@VUB.HR');
Insert into STUDENTI (JMBAG,PREZIME,IME,REDOVNI,GODISTE,SPOL,POZMOB,MOBITEL,EMAIL) values (12373,'Reš¡tarović','Karmela',1,1997,1,97,268951,'KRESTAROVIC@VUB.HR');
Insert into STUDENTI (JMBAG,PREZIME,IME,REDOVNI,GODISTE,SPOL,POZMOB,MOBITEL,EMAIL) values (12374,'Klisović','Sanja',1,1994,1,92,547847,'JKLISOVIC@VUB.HR');
Insert into STUDENTI (JMBAG,PREZIME,IME,REDOVNI,GODISTE,SPOL,POZMOB,MOBITEL,EMAIL) values (12375,'Kunović','Damir',1,1995,0,98,647894,'DKUNOVIC@VUB.HR');
Insert into STUDENTI (JMBAG,PREZIME,IME,REDOVNI,GODISTE,SPOL,POZMOB,MOBITEL,EMAIL) values (12376,'Hundrić','Stjepan',1,1995,0,98,325861,'SHUNDRIC@VUB.HR');
Insert into STUDENTI (JMBAG,PREZIME,IME,REDOVNI,GODISTE,SPOL,POZMOB,MOBITEL,EMAIL) values (12377,'Krnjić','Marina',1,1996,1,91,265875,'MKRNJIC@VUB.HR');

--primjer3
--dohvaćanje studenta
select * from studenti where prezime = 'Vukoja' and ime = 'Oliver';

--primjer4 kreiranje indeksa
create index ix_prezimena on studenti
   (prezime, ime);
   
--primjer5 dohvaćanje podatka preko hinta
select /*+ index(studenti ix_prezimena) */ * from studenti where prezime = 'Vukoja' and ime = 'Oliver';

--primjer6 dohvaćanje podataka preko polja koja nisu u indeksu
select * from studenti where email = 'OVUKOJA@VUB.HR'; 

--primjer 7 dodavanje unique constrainta i automatsko dodavanje indexa
ALTER TABLE studenti
ADD CONSTRAINT uk_email UNIQUE (email);

--primjer 8 dohvaćanje podataka preko automatski generiranog indeksa prilikom definicije unique constrainta
select * from studenti where email = 'OVUKOJA@VUB.HR'; 

--primjer 9 dohvaćanje podataka i case sensitive upiti
select * from studenti where prezime = 'Vukoja' and ime = 'Oliver'

--primjer 10 dohvaćanje podataka i case sensitive upiti
select * from studenti where prezime = 'VUKOJA' and ime = 'OLIVER'

--primjer 11 dohvaćanje podataka preko UPPER funkcije
select 
   * 
from 
   studenti 
where 
   upper(prezime) = UPPER('VuKoJa') and 
   upper(ime) = UPPER('OlIvEr');

--primjer 12 dohvaćanje podataka preko UPPER funkcije koristeći hint
select 
   /*+ index(studenti ix_prezimena) */ * 
from 
   studenti 
where 
   upper(prezime) = UPPER('VuKoJa') and 
   upper(ime) = UPPER('OlIvEr');
   
--primjer 13 kreiranje function base indeksa
create index ix_prezimena_f on studenti
   (upper(prezime), upper(ime));








