drop table UPISI;
drop table PREDMETI;
drop table NASTAVNICI;
drop table STUDENTI;

drop sequence STUDENTI_ID_SEQ;
drop sequence PREDMETI_ID_SEQ;
drop sequence NASTAVNICI_ID_SEQ;
drop sequence UPISI_ID_SEQ;


CREATE TABLE STUDENTI (
	ID NUMBER(9, 0) NOT NULL,
	JMBAG NUMBER(8, 0) UNIQUE NOT NULL,
	ime VARCHAR2(20) NOT NULL,
	prezime VARCHAR2(20) NOT NULL,
	email VARCHAR2(20) UNIQUE NOT NULL,
	password VARCHAR2(20) NOT NULL,
	spol NUMBER(1, 0) NOT NULL,
    CONSTRAINT ck_studenti_spol
    CHECK (spol IN (0,1)),
	constraint STUDENTI_PK PRIMARY KEY (ID));

CREATE sequence STUDENTI_ID_SEQ;

CREATE trigger BI_STUDENTI_ID
  before insert on STUDENTI
  for each row
begin
  select STUDENTI_ID_SEQ.nextval into :NEW.ID from dual;
end;

/
CREATE TABLE NASTAVNICI (
	ID NUMBER(9, 0) NOT NULL,
	ime VARCHAR2(20) NOT NULL,
	prezime VARCHAR2(30) NOT NULL,
	email VARCHAR2(30) UNIQUE NOT NULL,
	password VARCHAR2(20) NOT NULL,
	spol NUMBER(1, 0) NOT NULL,
    CONSTRAINT ck_nastavnici_spol
    CHECK (spol IN (0,1)),
	constraint NASTAVNICI_PK PRIMARY KEY (ID));

CREATE sequence NASTAVNICI_ID_SEQ;

CREATE trigger BI_NASTAVNICI_ID
  before insert on NASTAVNICI
  for each row
begin
  select NASTAVNICI_ID_SEQ.nextval into :NEW.ID from dual;
end;

/
CREATE TABLE PREDMETI (
	ID NUMBER(9, 0) NOT NULL,
	IDnastavnika NUMBER(9, 0) NOT NULL,
	sifra VARCHAR2(10) UNIQUE NOT NULL,
	naziv VARCHAR2(30) NOT NULL,
	ects NUMBER(1, 0) NOT NULL,
	semestar NUMBER(1, 0) NOT NULL,
    CONSTRAINT ck_predmeti_semestar
    CHECK (semestar IN (1, 2, 3, 4, 5, 6)),
	constraint PREDMETI_PK PRIMARY KEY (ID));

CREATE sequence PREDMETI_ID_SEQ;

CREATE trigger BI_PREDMETI_ID
  before insert on PREDMETI
  for each row
begin
  select PREDMETI_ID_SEQ.nextval into :NEW.ID from dual;
end;

/
CREATE TABLE UPISI (
	ID NUMBER(9, 0) NOT NULL,
	IDpredmeta NUMBER(9, 0) NOT NULL,
	IDstudenta NUMBER(9, 0) NOT NULL,
	datum DATE NOT NULL,
	ocjena NUMBER(1, 0) NULL,
    CONSTRAINT ck_upisi_ocjena
    CHECK (ocjena IN (1, 2, 3, 4, 5)),
	constraint UPISI_PK PRIMARY KEY (ID));

CREATE sequence UPISI_ID_SEQ;

CREATE trigger BI_UPISI_ID
  before insert on UPISI
  for each row
begin
  select UPISI_ID_SEQ.nextval into :NEW.ID from dual;
end;

/


ALTER TABLE PREDMETI 
ADD CONSTRAINT PREDMETI_fk0 
    FOREIGN KEY (IDnastavnika) 
    REFERENCES NASTAVNICI(ID);

ALTER TABLE UPISI 
ADD CONSTRAINT UPISI_fk0 
    FOREIGN KEY (IDpredmeta) 
    REFERENCES PREDMETI(ID);
    
ALTER TABLE UPISI 
ADD CONSTRAINT UPISI_fk1 
    FOREIGN KEY (IDstudenta) 
    REFERENCES STUDENTI(ID);


INSERT into STUDENTI (JMBAG, ime, prezime, email, password, spol) values (12345, 'Marko', 'Marković', 'mmarkovic@vub.hr', 'zaporka123', 0); 
INSERT into STUDENTI (JMBAG, ime, prezime, email, password, spol) values (12346, 'Petar', 'Petrović', 'ppetrovic@vub.hr', 'zaporka123', 0);
INSERT into STUDENTI (JMBAG, ime, prezime, email, password, spol) values (12347, 'Marija', 'Jonjić', 'mjonjic@vub.hr', 'zaporka123', 1);
INSERT into STUDENTI (JMBAG, ime, prezime, email, password, spol) values (12348, 'Ana', 'Kalus', 'akalus@vub.hr', 'zaporka123', 1);
INSERT into STUDENTI (JMBAG, ime, prezime, email, password, spol) values (12349, 'Ivan', 'Radić', 'iradic@vub.hr', 'zaporka123', 0);
INSERT into STUDENTI (JMBAG, ime, prezime, email, password, spol) values (12350, 'Darko', 'Gogoljak', 'dgogoljak@vub.hr', 'zaporka123', 0);
INSERT into STUDENTI (JMBAG, ime, prezime, email, password, spol) values (12351, 'Antun', 'Sobljak', 'asobljak@vub.hr', 'zaporka123', 1);
INSERT into STUDENTI (JMBAG, ime, prezime, email, password, spol) values (12352, 'Josipa', 'Major', 'jmajor@vub.hr', 'zaporka123', 1);
INSERT into STUDENTI (JMBAG, ime, prezime, email, password, spol) values (12353, 'Mia', 'Zmajar', 'mzmajar@vub.hr', 'zaporka123', 1);
INSERT into STUDENTI (JMBAG, ime, prezime, email, password, spol) values (12354, 'Josip', 'Hundrić', 'jhundric@vub.hr', 'zaporka123', 0);


INSERT into NASTAVNICI (ime, prezime, email, password, spol) values ('Romana', 'Knežić', 'rknezic@vub.hr', 'zaporka123', 1);
INSERT into NASTAVNICI (ime, prezime, email, password, spol) values ('Saša', 'Kopljar', 'skopljar@vub.hr', 'zaporka123', 0);
INSERT into NASTAVNICI (ime, prezime, email, password, spol) values ('Petar', 'Pereža', 'ppereza@vub.hr', 'zaporka123', 0);


INSERT into PREDMETI (IDnastavnika, sifra, naziv, ects, semestar) values (1, 111, 'Matematika 2', 7, 2);
INSERT into PREDMETI (IDnastavnika, sifra, naziv, ects, semestar) values (2, 112, 'Uvod u programiranje', 6, 1);
INSERT into PREDMETI (IDnastavnika, sifra, naziv, ects, semestar) values (3, 113, 'Digitalna tehnika', 6, 3);


INSERT into UPISI (IDpredmeta, IDstudenta, datum, ocjena) values (1, 2, sysdate, null);
INSERT into UPISI (IDpredmeta, IDstudenta, datum, ocjena) values (1, 1, sysdate, 3);
INSERT into UPISI (IDpredmeta, IDstudenta, datum, ocjena) values (2, 4, sysdate, 2);
INSERT into UPISI (IDpredmeta, IDstudenta, datum, ocjena) values (1, 7, sysdate, null);
INSERT into UPISI (IDpredmeta, IDstudenta, datum, ocjena) values (2, 7, sysdate, null);
INSERT into UPISI (IDpredmeta, IDstudenta, datum, ocjena) values (3, 7, sysdate, null);
