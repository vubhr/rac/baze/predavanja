create or replace NONEDITIONABLE PACKAGE DOHVAT AS 
  e_iznimka exception;
  procedure p_get_zupanije(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
  procedure p_login(in_json in json_object_t, out_json out json_object_t);

END DOHVAT;