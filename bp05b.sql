--primjer 1 priprema podataka

DROP TABLE STUDENTI;
DROP SEQUENCE STUDENTI_ID_SEQ;
DROP TABLE VUBSTUDENTI;

CREATE TABLE studenti (
	ID NUMBER(9,0) NOT NULL,
	JMBAG NUMBER(8,0) NOT NULL,
	PREZIME VARCHAR2(20) NOT NULL,
	IME VARCHAR2(20) NOT NULL,
	REDOVNI NUMBER(1,0) NOT NULL,
	GODISTE NUMBER(4,0) NOT NULL,
	SPOL NUMBER(1,0) NOT NULL,
	POZMOB NUMBER(3,0) NOT NULL,
	MOBITEL NUMBER(7,0) NOT NULL,
	EMAIL VARCHAR2(40) NOT NULL,
	constraint STUDENTI_PK PRIMARY KEY (ID));

CREATE sequence STUDENTI_ID_SEQ;

CREATE trigger BI_STUDENTI_ID
  before insert on studenti
  for each row
begin
  select STUDENTI_ID_SEQ.nextval into :NEW.ID from dual;
end;

/

Insert into STUDENTI (JMBAG,PREZIME,IME,REDOVNI,GODISTE,SPOL,POZMOB,MOBITEL,EMAIL) values (12367,'Vukoja','Oliver',1,1997,0,98,587451,'OVUKOJA@VUB.HR');
Insert into STUDENTI (JMBAG,PREZIME,IME,REDOVNI,GODISTE,SPOL,POZMOB,MOBITEL,EMAIL) values (12368,'Zorić','Tomislav',1,1995,0,98,685973,'TZORIC@VUB.HR');
Insert into STUDENTI (JMBAG,PREZIME,IME,REDOVNI,GODISTE,SPOL,POZMOB,MOBITEL,EMAIL) values (12369,'Kopčić','Josip',1,1997,0,95,574123,'JKOPCIC@VUB.HR');
Insert into STUDENTI (JMBAG,PREZIME,IME,REDOVNI,GODISTE,SPOL,POZMOB,MOBITEL,EMAIL) values (12370,'Marinić','Jelena',1,1996,1,91,564873,'JMARINIC@VUB.HR');
Insert into STUDENTI (JMBAG,PREZIME,IME,REDOVNI,GODISTE,SPOL,POZMOB,MOBITEL,EMAIL) values (12371,'Arnold','Mirela',1,1994,1,91,147985,'MARNOLD@VUB.HR');
Insert into STUDENTI (JMBAG,PREZIME,IME,REDOVNI,GODISTE,SPOL,POZMOB,MOBITEL,EMAIL) values (12373,'Reštarović','Karmela',1,1997,1,97,268951,'KRESTAROVIC@VUB.HR');
Insert into STUDENTI (JMBAG,PREZIME,IME,REDOVNI,GODISTE,SPOL,POZMOB,MOBITEL,EMAIL) values (12374,'Klisović','Sanja',1,1994,1,92,547847,'JKLISOVIC@VUB.HR');
Insert into STUDENTI (JMBAG,PREZIME,IME,REDOVNI,GODISTE,SPOL,POZMOB,MOBITEL,EMAIL) values (12375,'Kunović','Damir',1,1995,0,98,647894,'DKUNOVIC@VUB.HR');
Insert into STUDENTI (JMBAG,PREZIME,IME,REDOVNI,GODISTE,SPOL,POZMOB,MOBITEL,EMAIL) values (12376,'Hundrić','Stjepan',1,1995,0,98,325861,'SHUNDRIC@VUB.HR');
Insert into STUDENTI (JMBAG,PREZIME,IME,REDOVNI,GODISTE,SPOL,POZMOB,MOBITEL,EMAIL) values (12377,'Krnjić','Marina',1,1996,1,91,265875,'MKRNJIC@VUB.HR');

--primjer 2 opis tablice i dohvaćanje svih vrijednosti iz tablice
desc studenti;
select * from studenti;

--primjer 3 dohvaćanje podataka iz tablice
--Dohvatite sve studente koji su 1997 godište 
select * from studenti where godiste = 1997;

--matematičke operacije nad podacima u poljima
--Dohvatite ime, prezime i godine starost za sve muške studente
SELECT ime, prezime, (2020 - GODISTE) FROM STUDENTI WHERE SPOL = 0;

--sysdate 
select sysdate from dual;

--konverzija sysdate u karakter
select to_char(sysdate, 'YYYY/MM/DD')as datum from dual;

--konverzija sysdate u number, prethodno mora ići u karakter
select to_number(to_char(sysdate, 'YYYYMMDD'))as datum from dual;

--dohvaćanje samo sistemske godine
select to_number(to_char(sysdate, 'YYYY'))as datum from dual;

--ovo se ne može konvertirati u number 
select to_number(to_char(sysdate, 'YYYY.MM.DD'))as datum from dual;

--Matematičke operacije u SQLu
select 5*(13-1) as matematika from dual;

--Dohvatite ime i prezime studenata mlađih od 25 godina
SELECT 
   ime, prezime, (to_number(to_char(sysdate, 'YYYY')) - godiste) starost  
from 
   studenti 
where 
   (to_number(to_char(sysdate, 'YYYY')) - godiste ) <= 24;

--dohvaćanje jedinstvenih vrijednosti unutar jednog polja
--Koje sve mreže mobitela koriste žene?
SELECT DISTINCT pozmob FROM studenti where spol = 1;

--spajanje polja
--Dohvatite ime i prezime studenata (ime i prezime treba spojiti), treba spojiti i pozivni broj mobitela i mobitel.
select ime || ' ' || prezime as "ime i prezime", '0' || pozmob || ' ' || mobitel as "broj mobitela" from studenti;

--case izraz za zamjenu vrijednosti, drugačija interpretacija podataka
--Dohvatite ime i prezime studenta (spojite polja ime i prezime) i ako je spol = 0 ispišite muško, a ako je 1 ispišite žensko
select 
   ime || ' ' || prezime as "ime i prezime",
case spol
when 0 then
  'muško'
when 1 then
  'žensko'
else
   null
end as "naziv spola"
from studenti;

--dohvaćanje podataka unutar nekog raspona range
--Dohvatite sve studente kojima je godište između 1995. i 1997.
select 
   ime, prezime, godiste 
from 
   studenti 
where 
   godiste between 1995 and 1997;
   
--dohvaćanje samo nekoliko određenih vrijednosti
--Dohvatite sve studente koji su ili 1995. ili 1997. godište
select 
   ime, prezime, godiste 
from 
   studenti 
where 
   godiste in (1995, 1997);
--ili
select 
   ime, prezime, godiste 
from 
   studenti 
where 
   godiste = 1995 OR 
   godiste = 1997;
   
--dohvaćanje svih zapisa osim nekoliko određenih
--Dohvatite sve studente koji nisu 1995 ili 1997. godište
select 
   ime, prezime, godiste 
from 
   studenti 
where 
   godiste not in (1995, 1997);
-- ili
select 
   ime, prezime, godiste 
from 
   studenti 
where 
   godiste != 1995 AND 
   godiste != 1997;
   
--dohvaćanje zapisa koji u varchar polju imaju određeni slijed znakova
--Dohvatite sve studente kojima ime počinje sa slovom S
select 
   * 
from 
   studenti 
where 
   ime like 'S%';
   
--dohvaćanje zapisa koji imaju uvjet po određenom broju znakova
--Dohvatite sve studente kojima je u imenu na drugom mjestu slovo a
select 
  * 
from 
  studenti
where 
  ime like '_a%'



   








