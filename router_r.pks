create or replace NONEDITIONABLE PACKAGE ROUTER AS 
 e_iznimka exception;
 PRAGMA EXCEPTION_INIT(e_iznimka, -20001); 
    
 procedure p_main(p_in in clob, p_out out clob);

 END ROUTER;